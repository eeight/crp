#include "mmap.h"
#include "query.h"
#include "range.h"
#include "range.h"
#include "stored_graph.h"
#include "stored_metric.h"
#include "test_graph.h"

#include <mms/cast.h>

#include <boost/format.hpp>
#include <boost/progress.hpp>

int main(int argc, const char** argv) {
    if (argc != 5 && argc != 1) {
        std::cout << "Usage: " << argv[0] <<
            " [graph overlay metric prepro]\n";
        return 1;
    }

    const char* DEFAULT_PATHS[] = {
        "",
        "genfiles/graph.mms",
        "genfiles/graph_overlay.mms",
        "genfiles/graph_metric.mms",
        "genfiles/graph_prepro.mms"};

    if (argc == 1) {
        argv = DEFAULT_PATHS;
    }

    std::unique_ptr<Graph> graph;
    if (argv[1] == std::string("-")) {
        graph.reset(new TestGraph);
    } else {
        graph.reset(new StoredGraph(argv[1]));
    }
    Mmap overlayMapping(argv[2]);
    std::unique_ptr<Metric> metric;
    if (argv[3] == std::string("-")) {
        metric.reset(new TestMetric);
    } else {
        metric.reset(new StoredMetric(argv[3]));
    }
    StoredPrepro prepro(argv[4]);

    auto overlay = mms::safeCast<Overlay>(
            overlayMapping.address(), overlayMapping.size());

    Query query(graph.get(), &overlay);
    for (size_t i = 0; i != 100; ++i) {
        OriginalEdgeId u(rand() % graph->edgesNumber().untag());
        OriginalEdgeId v(rand() % graph->edgesNumber().untag());
        boost::progress_timer timer;
        std::cout << "From: " << graph->edge(u).id.untag() <<
            " To: " << graph->edge(v).id.untag() << '\n';
        const auto& optPath = query.queryPath(*metric, prepro, u, v);
        if (optPath && !optPath->empty()) {
            const auto path = *optPath;
            if (path.front() != u) {
                std::cerr << "wrong start\n";
                throw 1;
            }
            if (path.back() != v) {
                std::cerr << "wrong finish\n";
                throw 1;
            }
            for (size_t i = 1; i != path.size(); ++i) {
                bool found = false;
                for (const auto& e: graph->outEdges(
                            graph->edge(path[i - 1]).target)) {
                    if (e.id == path[i]) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    std::cerr << "Impossible transition: " <<
                        path[i - 1].untag() << " => " << path[i].untag() <<
                        std::endl;
                    throw 1;
                }
            }
            std::cout << "Time: " << metric->cost(path) << std::endl;
        }
    }
    return 0;
}
