#pragma once

#include "graph.h"
#include "metric.h"

TaggedVector<OriginalEdgeTag, TaggedVector<OriginalEdgeTag, float>>
fw(const Graph& graph, const Metric& metric);
