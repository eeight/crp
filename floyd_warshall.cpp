#include "floyd_warshall.h"

#include "range.h"

TaggedVector<OriginalEdgeTag, TaggedVector<OriginalEdgeTag, float>>
fw(const Graph& graph, const Metric& metric) {
    TaggedVector<OriginalEdgeTag, TaggedVector<OriginalEdgeTag, float>> costs(
            graph.edgesNumber(),
            TaggedVector<OriginalEdgeTag, float>(
                graph.edgesNumber(), std::numeric_limits<float>::infinity()));

    for (const auto& e: graph.edges()) {
        costs[e.id][e.id] = 0;
        for (const auto& outE: graph.outEdges(e.target)) {
            costs[e.id][outE.id] = metric.turnCost(e.id, outE.id) +
                metric.cost(outE.id);
        }
    }

    for (const auto k: range(graph.edgesNumber())) {
        for (const auto i: range(graph.edgesNumber())) {
            for (const auto j: range(graph.edgesNumber())) {
                costs[i][j] = std::min(
                    costs[i][j], costs[i][k] + costs[k][j]);
            }
        }
    }

    return costs;
}
