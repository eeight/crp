#pragma once

#include "dense_hash_map.h"
#include "dense_hash_set.h"
#include "graph.h"
#include "heap.h"
#include "metric.h"
#include "overlay.h"
#include "prepro.h"
#include "tagged.h"

#include <boost/optional.hpp>

#include <cassert>

struct DijkstraKeyTag;
using DijkstraKey = Tagged<DijkstraKeyTag, uint32_t>;

class DijkstraCodec {
public:
    DijkstraCodec(
            OverlayVertexId overlayVerticesNumber,
            OriginalEdgeId originalEdgesNumber) :
        overlayVerticesNumber_(overlayVerticesNumber.untag()),
        originalEdgesNumber_(originalEdgesNumber.untag())
    {}

    DijkstraKey size() const {
        return DijkstraKey(
                overlayVerticesNumber_ + originalEdgesNumber_);
    }

    DijkstraKey encode(OverlayVertexId v) const {
        assert(v.untag() < overlayVerticesNumber_);
        return retag<DijkstraKeyTag>(v);
    }

    DijkstraKey encode(OriginalEdgeId e) const {
        assert(e.untag() < originalEdgesNumber_);
        return DijkstraKey(e.untag() + overlayVerticesNumber_);
    }

    bool isOverlayVertex(DijkstraKey x) const {
        return x.untag() < overlayVerticesNumber_;
    }

    OverlayVertexId overlayVertexId(DijkstraKey x) const {
        assert(x.untag() < overlayVerticesNumber_);
        return OverlayVertexId(x.untag());
    }

    OriginalEdgeId originalEdgeId(DijkstraKey x) const {
        assert(x.untag() >= overlayVerticesNumber_);
        assert(x < size());
        return OriginalEdgeId(x.untag() - overlayVerticesNumber_);
    }
private:
    uint32_t overlayVerticesNumber_;
    uint32_t originalEdgesNumber_;
};

class DijkstraStorage {
    DijkstraStorage(const Graph* graph, const Overlay* overlay) :
        graph_(graph),
        overlay_(overlay),
        codec_(overlay->overlayVerticesNumber(), graph->edgesNumber())
    {}

private:
    friend class DijkstraBase;

    template <class>
    friend class Dijkstra;

    template <class>
    friend class ReverseDijkstra;

    friend class Query;

    void reset();

    template <class Policy>
    std::vector<std::pair<LevelId, DijkstraKey>> backtrack(
            DijkstraKey sourceKey,
            DijkstraKey targetKey,
            Policy policy) const;

    ComponentVector componentVector(DijkstraKey key) const;

    const Graph* graph_;
    const Overlay* overlay_;
    const DijkstraCodec codec_;

    struct DijkstraState {
        float cost = std::numeric_limits<float>::infinity();
        DijkstraKey backlink = std::numeric_limits<DijkstraKey>::max();
    };
    DenseHashMap<DijkstraKey, DijkstraState> states_;
    DenseHashSet<DijkstraKey> black_;

    Heap<DijkstraKey> heap_;
};

enum class LevelPolicy {
    Zero,
    Nonzero,
    Any
};

struct ShortestPathPolicy {
    static constexpr LevelPolicy level = LevelPolicy::Any;

    bool isFrontier(OverlayVertexId) const { return false; }

    LevelId componentLevel(ComponentVector cv) const {
        return std::min(commonLevel(first, cv), commonLevel(second, cv));
    }

    bool isZeroLevelComponent(ComponentVector cv) const {
        return cv == first || cv == second;
    }

    ComponentVector first, second;
};

struct UnpackZeroLevelPolicy {
    static constexpr LevelPolicy level = LevelPolicy::Zero;

    bool isFrontier(OverlayVertexId) const { return false; }

    LevelId componentLevel(ComponentVector) const {
        return 0;
    }

    bool isZeroLevelComponent(const ComponentVector cv) const {
        return cv == this->cv;
    }

    ComponentVector cv;
};

struct UnpackNonzeroLevelPolicy {
    static constexpr LevelPolicy level = LevelPolicy::Nonzero;

    bool isFrontier(OverlayVertexId v) const {
        return v >= from && v < to;
    }

    LevelId componentLevel(ComponentVector) const {
        return fixedLevel;
    }

    bool isZeroLevelComponent(ComponentVector) const {
        return false;
    }

    LevelId fixedLevel;
    // Due to the way that overlay vertices are numbered
    // we can quickly check if the vertex belongs to the
    // needed component frontier by just checking if it
    // belongs to the range.
    // See [Overlay vertices numbering] in overlay.h
    OverlayVertexId from, to;
};

class DijkstraBase {
public:
    DijkstraBase(
            const Metric* metric,
            const Prepro* prepro,
            DijkstraStorage* storage,
            DijkstraKey sourceKey,
            DijkstraKey targetKey,
            DijkstraKey startKey,
            std::function<void (DijkstraKey, float)> onRelax);
protected:
    boost::optional<std::pair<DijkstraKey, float>> initialState();
    boost::optional<std::pair<DijkstraKey, float>> popHeap();

    void relax(DijkstraKey prev, DijkstraKey key, float newCost);

    const Graph* graph_;
    const Overlay* overlay_;
    const Metric* metric_;
    const Prepro* prepro_;
    DijkstraCodec codec_;
    DijkstraStorage* storage_;
    DijkstraKey sourceKey_;
    DijkstraKey targetKey_;
    DijkstraKey key_;
    float cost_;
    std::function<void (DijkstraKey, float)> onRelax_;
    bool initialState_ = true;
};

template <class Policy>
class Dijkstra : private DijkstraBase {
public:
    Dijkstra(
            const Metric* metric,
            const Prepro* prepro,
            DijkstraStorage* storage,
            DijkstraKey sourceKey,
            DijkstraKey targetKey,
            Policy policy,
            std::function<void (DijkstraKey, float)> onRelax);

    void relaxOriginalEdge(OriginalEdgeId edgeId);

    // Overlay out vertices have only one outgoing edge each.
    // So when we relax them, we don't put them on the heap and immediately
    // relax target vertex of the edge.
    void relaxOverlayOutVertex(
            DijkstraKey prev,
            OverlayVertexId v,
            float newCost);

    void relaxOverlayInVertex(
            OverlayVertexId v, ComponentVector componentVector);

    boost::optional<std::pair<DijkstraKey, float>> next();

private:
    Policy policy_;
};

template <class Policy>
class ReverseDijkstra : private DijkstraBase {
public:
    ReverseDijkstra(
            const Metric* metric,
            const Prepro* prepro,
            DijkstraStorage* storage,
            DijkstraKey sourceKey,
            DijkstraKey targetKey,
            Policy policy,
            std::function<void (DijkstraKey, float)> onRelax);

    void relaxOriginalEdge(OriginalEdgeId edgeId);

    // Overlay out vertices have only one outgoing edge each.
    // So when we relax them, we don't put them on the heap and immediately
    // relax target vertex of the edge.
    void relaxOverlayInVertex(
            DijkstraKey prev,
            OverlayVertexId v,
            float newCost);

    void relaxOverlayOutVertex(
            OverlayVertexId v, ComponentVector componentVector);

    boost::optional<std::pair<DijkstraKey, float>> next();

private:
    Policy policy_;
};
