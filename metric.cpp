#include "metric.h"

float Metric::cost(const std::vector<OriginalEdgeId>& path) const {
    if (path.empty()) {
        return 0;
    }

    float w = 0;
    for (size_t i = 1; i != path.size(); ++i) {
        w += turnCost(path[i - 1], path[i]);
        w += cost(path[i]);
    }

    return w;
}
