#pragma once

#include "tagged.h"
#include "range.h"

#include <mms/vector.h>

#include <boost/lexical_cast.hpp>

#include <string>

class Printer {
public:
    void begin(const std::string& name);
    void end();
    void key(const std::string& key);
    void value(const std::string& value);

    template <class Tag, class T>
    void key(const Tagged<Tag, T>& k) {
        key(boost::lexical_cast<std::string>(k.untag()));
    }

    template <class T>
    void key(const T& k) {
        key(boost::lexical_cast<std::string>(k));
    }

    template <class P, class Key, class Value>
    void value(const mms::map<P, Key, Value>& m) {
        begin("map");
        for (const auto& kv: m) {
            key(kv.first);
            value(kv.second);
        }
        end();
    }

    template <class P, class T>
    void value(const mms::vector<P, T>& v) {
        begin("vector");
        for (size_t i = 0; i != v.size(); ++i) {
            key(i);
            value(v[i]);
        }
        end();
    }

    template <class P, class Tag, class T>
    void value(const TaggedMmsVector<P, Tag, T>& v) {
        begin("vector");
        for (const auto i: range(v.size())) {
            key(i);
            value(v[i]);
        }
        end();
    }

    template <class Tag, class T>
    void value(const Tagged<Tag, T>& v) {
        value(boost::lexical_cast<std::string>(v.untag()));
    }

    template <class T>
    void value(const T& v) {
        value(boost::lexical_cast<std::string>(v));
    }

private:
    void indent() const;
    int indent_ = 0;
};
