#include "partition.h"
#include "range.h"
#include "dense_hash_map.h"

#include <cassert>

namespace {

class Remapping {
public:
    Skeleton skeleton() && {
        skeleton_.edges.shrink_to_fit();
        mapping_.shrink();
        return std::move(skeleton_);
    }

    void addVertex(OriginalVertexId v) {
        assert(!mapping_.count(v));
        mapping_.emplace(v, skeleton_.verticesNumber++);
        unmapping_.push_back(v);
    }

    void addEdge(OriginalVertexId u, OriginalVertexId v) {
        skeleton_.edges.emplace_back(remap(u), remap(v));
    }

    OriginalVertexId unmap(OriginalVertexId id) const {
        return unmapping_.at(id);
    }

    OriginalVertexId verticesNumber() const {
        return skeleton_.verticesNumber;
    }
private:
    OriginalVertexId remap(OriginalVertexId v) {
        return mapping_.at(v);
    }

    Skeleton skeleton_;
    DenseHashMap<OriginalVertexId, OriginalVertexId> mapping_;
    TaggedVector<OriginalVertexTag, OriginalVertexId> unmapping_;
};

template <class Getter>
OneLevelPartition partitionOneLevel(
        const Skeleton& skeleton,
        Getter prevPartition,
        size_t partsNumber,
        const Partitioner& partitioner) {
    std::cout << "Remapping edges...\n";
    TaggedVector<GraphComponentTag, Remapping> mappings;
    size_t crossEdges = 0;
    for (const auto v: range(skeleton.verticesNumber)) {
        const auto part = prevPartition(v);
        if (part >= mappings.size()) {
            mappings.resize(part.succ());
        }
        mappings[part].addVertex(v);
    }
    for (const auto& edge: skeleton.edges) {
        const auto part = prevPartition(edge.first);
        if (prevPartition(edge.second) != part) {
            ++crossEdges;
            continue;
        }
        mappings[part].addEdge(edge.first, edge.second);
    }
    std::cout << "Done, " << mappings.size().untag() << " parts, " <<
        crossEdges << " cross edges\n";
    std::cout << "Running partitioner...\n";
    OneLevelPartition result;
    result.resize(skeleton.verticesNumber, 0);
    GraphComponentId partsBefore = 0;
    for (auto& mapping: mappings) {
        const auto& parts = partitioner(
                std::move(mapping).skeleton(), partsNumber);
        for (const auto i: range(parts.size())) {
            assert(parts[i].untag() < partsNumber);
            result[mapping.unmap(i)] = partsBefore + parts[i];
        }
        partsBefore = partsBefore +
                std::max_element(parts.begin(), parts.end())->succ();
    }
    std::cout << "Done.\n";

    return result;
}

} // namespace

Partitions multilevelPartition(
        const Skeleton& skeleton,
        const std::vector<int>& scheme,
        const Partitioner& partitioner) {
    Partitions result;
    for (size_t i = 0; i != scheme.size(); ++i) {
        result.push_back(i > 0 ?
            partitionOneLevel(
                    skeleton,
                    [&result, i](OriginalVertexId v) {
                        return result[LevelId(i - 1)][v];
                    },
                    scheme[i],
                    partitioner) :
            partitionOneLevel(
                    skeleton,
                    [](OriginalVertexId) { return GraphComponentId(0u); },
                    scheme[i],
                    partitioner));
    }

    std::reverse(result.begin(), result.end());
    return result;
}
