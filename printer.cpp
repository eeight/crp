#include "printer.h"

#include <iostream>

void Printer::begin(const std::string& name) {
    std::cout << name << " {\n";
    ++indent_;
}

void Printer::end() {
    --indent_;
    indent();
    std::cout << "}\n";
}

void Printer::key(const std::string& key) {
    indent();
    std::cout << key << ": ";
}

void Printer::value(const std::string& value) {
    std::cout << value << "\n";
}

void Printer::indent() const {
    std::cout << std::string(indent_*4, ' ');
}
