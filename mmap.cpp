#include "mmap.h"

#include <stdexcept>

#include <sys/fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

Mmap::Mmap(const std::string& filename) {
    int fd = open(filename.c_str(), 0, O_RDONLY);
    if (fd < 0) {
        throw std::runtime_error("Cannot open " + filename);
    }
    struct stat st;
    if (fstat(fd, &st) < 0) {
        close(fd);
        throw std::runtime_error("Cannot stat " + filename);
    }
    size_ = st.st_size;
    void* result = mmap(nullptr, size_, PROT_READ, MAP_SHARED, fd, 0);
    close(fd);
    if (result == (void *)-1) {
        throw std::runtime_error("Cannot mmap " + filename);
    }

    address_ = (const char *)result;
}

Mmap::~Mmap() {
    munmap((void *)address_, size_);
}
