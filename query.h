#pragma once

#include "dijkstra.h"
#include "graph.h"
#include "metric.h"
#include "overlay.h"
#include "path_cache.h"
#include "prepro.h"

#include <boost/optional.hpp>

#include <vector>
#include <memory>

class Query {
public:
    Query(const Graph* graph, const Overlay* overlay);

    // NOTE [Positions on graph]
    // ~~~~~~~~~~~~~~~~~~~~~~~~~
    // Position 'on edge #id' means we are at the end of the said edge.
    float queryCost(
            const Metric& metric,
            const Prepro& prepro,
            OriginalEdgeId source,
            OriginalEdgeId target);

    boost::optional<std::vector<OriginalEdgeId>> queryPath(
            const Metric& metric,
            const Prepro& prepro,
            OriginalEdgeId source,
            OriginalEdgeId target);
private:
    template <class Policy>
    std::pair<float, std::vector<std::pair<LevelId, DijkstraKey>>> query(
            const Metric& metric,
            const Prepro& prepro,
            DijkstraKey sourceKey,
            DijkstraKey targetKey,
            Policy forwardPolicy,
            Policy reversePolicy);

    std::vector<OriginalEdgeId> unpackShortcut(
            const Metric& metric,
            const Prepro& prepro,
            OverlayVertexId u,
            OverlayVertexId v,
            LevelId level);

    std::vector<OriginalEdgeId> decodePath(
            const Metric& metric,
            const Prepro& prepro,
            const std::vector<std::pair<LevelId, DijkstraKey>>& encodedPath);

    DijkstraCodec codec_;
    DijkstraStorage storage_;
    DijkstraStorage reverseStorage_;
    PathCache pathCache_;
};
