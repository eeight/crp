#pragma once

#include <cstddef>
#include <functional>
#include <limits>

#include <mms/vector.h>

template <class Tag, class T>
class Tagged {
public:
    explicit Tagged(T value) : value_(value)
    {}

    /* implicit */ Tagged(std::nullptr_t) : value_(0)
    {}

    template <class S>
    /* implicit */ Tagged(Tagged<Tag, S> value) : value_(value.untag())
    {}

    T untag() const { return value_; }
    bool operator == (const Tagged& other) const
        { return value_ == other.value_; }
    bool operator != (const Tagged& other) const
        { return value_ != other.value_; }
    bool operator < (const Tagged& other) const
        { return value_ < other.value_; }
    bool operator > (const Tagged& other) const
        { return value_ > other.value_; }
    bool operator <=(const Tagged& other) const
        { return value_ <= other.value_; }
    bool operator >=(const Tagged& other) const
        { return value_ >= other.value_; }

    Tagged& operator++() {
        ++value_;
        return *this;
    }

    Tagged operator++(int) {
        Tagged result = *this;
        ++value_;
        return result;
    }

    Tagged succ() const {
        return Tagged(value_ + 1);
    }

    Tagged pred() const {
        return Tagged(value_ - 1);
    }

    Tagged operator +(const Tagged& other) const {
        return Tagged(value_ + other.value_);
    }

    Tagged& operator +=(const Tagged& other) {
        value_ += other.value_;
        return *this;
    }

    Tagged operator -(const Tagged& other) const {
        return Tagged(value_ - other.value_);
    }

    template <class S>
    Tagged operator *(S mult) const {
        return Tagged(value_*mult);
    }

    template <class S>
    Tagged operator /(S div) const {
        return Tagged(value_/div);
    }

using MmappedType = Tagged;

private:
    T value_;
};

template <class P, class Tag, class T>
class TaggedMmsVector : public mms::vector<P, T>
{
public:
    using mms::vector<P, T>::vector;

    template<class Writer>
    size_t writeData(Writer& w) const {
        return mms::vector<P, T>::writeData(w);
    }

    template<class Writer>
    size_t writeField(Writer& w, size_t pos) const {
        return mms::vector<P, T>::writeField(w, pos);
    }

    using Index = Tagged<Tag, size_t>;

    T& operator[](Index i)
        { return mms::vector<P, T>::operator[](i.untag()); }

    const T& operator[](Index i) const
        { return mms::vector<P, T>::operator[](i.untag()); }

    T& at(Index i)
        { return mms::vector<P, T>::at(i.untag()); }

    const T& at(Index i) const
        { return mms::vector<P, T>::at(i.untag()); }

    Index size() const {
        return Index(mms::vector<P, T>::size());
    }

    void resize(Index size, T x = T{}) {
        mms::vector<P, T>::resize(size.untag(), x);
    }

    void reserve(Index size) {
        mms::vector<P, T>::reserve(size.untag());
    }
};

template <class Tag, class T>
class TaggedVector : public std::vector<T>
{
public:
    using Index = Tagged<Tag, size_t>;
    using reference = typename std::vector<T>::reference;
    using const_reference = typename std::vector<T>::const_reference;

    TaggedVector() = default;

    explicit TaggedVector(Index size, T init = T{}) :
        std::vector<T>(size.untag(), init)
    {}

    TaggedVector(std::initializer_list<T> list) :
        std::vector<T>(list)
    {}

    reference operator[](Index i)
        { return std::vector<T>::operator[](i.untag()); }

    const_reference operator[](Index i) const
        { return std::vector<T>::operator[](i.untag()); }

    reference at(Index i)
        { return std::vector<T>::at(i.untag()); }

    const_reference at(Index i) const
        { return std::vector<T>::at(i.untag()); }

    Index size() const {
        return Index(std::vector<T>::size());
    }

    void resize(Index size, T x = T{}) {
        std::vector<T>::resize(size.untag(), x);
    }

    void reserve(Index size) {
        std::vector<T>::reserve(size.untag());
    }

    void assign(Index size, T x) {
        std::vector<T>::assign(size.untag(), x);
    }
};

template <class Tag, class T>
Tagged<Tag, T> mkTagged(T t) {
    return Tagged<Tag, T>(t);
}

template <class TagB, class TagA, class T>
Tagged<TagB, T> retag(const Tagged<TagA, T>& tagged) {
    return mkTagged<TagB>(tagged.untag());
}

namespace std {

template<class Tag, class T>
struct is_trivial<Tagged<Tag, T>> : is_trivial<T> {};

template <class Tag, class T>
struct hash<Tagged<Tag, T>> {
public:
    size_t operator()(const Tagged<Tag, T>& v) const {
        return hash_(v.untag());
    }
private:
    hash<T> hash_;
};

template <class Tag, class T>
struct numeric_limits<Tagged<Tag, T>> {
    static Tagged<Tag, T> max() {
        return Tagged<Tag, T>(numeric_limits<T>::max());
    }

    static constexpr bool is_integer = numeric_limits<T>::is_integer;
    static constexpr bool is_specialized = true;
    static constexpr bool is_exact = numeric_limits<T>::is_exact;
    static constexpr bool radix = numeric_limits<T>::radix;
    static constexpr bool digits = numeric_limits<T>::digits;
    static constexpr bool max_digits10 = numeric_limits<T>::max_digits10;
};

} // namespace std
