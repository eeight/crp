#include "dijkstra.h"
#include "range.h"

#include <algorithm>
#include <limits>

void DijkstraStorage::reset() {
    heap_.clear();
    states_.clear();
    black_.clear();
}

template <class Policy>
std::vector<std::pair<LevelId, DijkstraKey>> DijkstraStorage::backtrack(
        DijkstraKey sourceKey, DijkstraKey targetKey, Policy policy) const {
    std::vector<std::pair<LevelId, DijkstraKey>>  path;
    for (DijkstraKey key = targetKey;
            key != sourceKey;
            key = states_.at(key).backlink) {
        path.emplace_back(policy.componentLevel(componentVector(key)), key);
    }
    path.emplace_back(
            policy.componentLevel(componentVector(sourceKey)), sourceKey);
    return path;
}

ComponentVector DijkstraStorage::componentVector(DijkstraKey key) const {
    return codec_.isOverlayVertex(key) ?
            overlay_->overlayVertexComponentVector(
                codec_.overlayVertexId(key)) :
            overlay_->vertexComponentVector(
                graph_->edge(codec_.originalEdgeId(key)).target);
}

DijkstraBase::DijkstraBase(
        const Metric* metric,
        const Prepro* prepro,
        DijkstraStorage* storage,
        DijkstraKey sourceKey,
        DijkstraKey targetKey,
        DijkstraKey startKey,
        std::function<void (DijkstraKey, float)> onRelax) :
    graph_(storage->graph_),
    overlay_(storage->overlay_),
    metric_(metric),
    prepro_(prepro),
    codec_(storage->codec_),
    storage_(storage),
    sourceKey_(sourceKey),
    targetKey_(targetKey),
    key_(startKey),
    cost_(0),
    onRelax_(std::move(onRelax))
{
    storage_->reset();
}

boost::optional<std::pair<DijkstraKey, float>> DijkstraBase::popHeap() {
    while (!storage_->heap_.empty()) {
        std::tie(cost_, key_) = storage_->heap_.pop();
        if (!storage_->black_.count(key_)) {
            storage_->black_.insert(key_);
            return {{key_, cost_}};
        }
    }
    return boost::none;
}

boost::optional<std::pair<DijkstraKey, float>> DijkstraBase::initialState() {
    if (initialState_) {
        initialState_ = false;
        onRelax_(key_, cost_);
        return {{key_, cost_}};
    } else {
        return {};
    }
}

void DijkstraBase::relax(DijkstraKey prev, DijkstraKey key, float newCost) {
    auto& state = storage_->states_[key];
    if (newCost < state.cost) {
        onRelax_(key, newCost);
        state.cost = newCost;
        state.backlink = prev;
        storage_->heap_.push(key, newCost);
    }
}

template <class Policy>
Dijkstra<Policy>::Dijkstra(
        const Metric* metric,
        const Prepro* prepro,
        DijkstraStorage* storage,
        DijkstraKey sourceKey,
        DijkstraKey targetKey,
        Policy policy,
        std::function<void (DijkstraKey, float)> onRelax) :
    DijkstraBase(
            metric,
            prepro,
            storage,
            sourceKey,
            targetKey,
            sourceKey,
            std::move(onRelax)),
    policy_(policy)
{
    storage_->states_[sourceKey].cost = 0;
    storage_->black_.insert(sourceKey);
}

template <class Policy>
void Dijkstra<Policy>::relaxOriginalEdge(OriginalEdgeId edgeId) {
    const auto edgeTargetId = graph_->edge(edgeId).target;
    const auto& outEdges = graph_->outEdges(edgeTargetId);

    const float* const penalties = prepro_->turnTable(edgeTargetId) +
        graph_->inEdgeIndex(edgeId).untag()*outEdges.size().untag();
    assert(policy_.isZeroLevelComponent(
                overlay_->vertexComponentVector(edgeTargetId)));
    for (const auto i: range(outEdges.size())) {
        const float penalty = penalties[i.untag()];
        const Edge& outEdge = outEdges[i];
        const auto componentVector =
            overlay_->vertexComponentVector(outEdge.target);
        if (policy_.isZeroLevelComponent(componentVector) ||
                codec_.encode(outEdge.id) == targetKey_) {
            relax(
                    key_,
                    codec_.encode(outEdge.id),
                    cost_ + penalty + metric_->cost(outEdge.id));
        } else if (Policy::level != LevelPolicy::Zero) {
            const auto outVertex =
                overlay_->originalEdgeToOverlayOutVertex(outEdge.id);
            relaxOverlayOutVertex(key_, outVertex, cost_ + penalty);
        }
    }
}

template <class Policy>
void Dijkstra<Policy>::relaxOverlayOutVertex(
        DijkstraKey prev,
        OverlayVertexId v,
        float newCost) {
    assert(storage_->overlay_->isOutVertex(v));
    const DijkstraKey key = codec_.encode(v);
    auto& state = storage_->states_[key];

    // This is 'relax' function hand-inlined here.
    if (newCost >= state.cost) {
        return;
    }
    state.cost = newCost;
    state.backlink = prev;
    const auto overlayEdge = storage_->overlay_->overlayOriginalEdge(v);
    onRelax_(key, newCost);
    relax(
        key,
        codec_.encode(overlayEdge.overlayVertexId),
        // TODO put original edges' cost into prepro.
        newCost + metric_->cost(overlayEdge.originalEdgeId));
}

template <class Policy>
void Dijkstra<Policy>::relaxOverlayInVertex(
        OverlayVertexId v, ComponentVector componentVector) {
    const auto levelSucc = policy_.componentLevel(componentVector);
    if (levelSucc == 0) {
        return;
    }
    const auto level = levelSucc.pred();
    const auto component = getComponentVector(level, componentVector);
    const auto& outVertices = overlay_->componentOutVertices(level, component);
    const size_t inIndex = overlay_->inVertexIndex(level, component, v);
    const float* const costsBase =
        prepro_->overlayCosts(overlay_->costsOffset(level, component));
    const float* const costs = costsBase +
        overlay_->componentOutVertices(level, component).size()*inIndex;
    for (size_t i = 0; i != outVertices.size(); ++i) {
        const float shortcutCost = costs[i];
        if (!policy_.isFrontier(outVertices[i])) {
            relaxOverlayOutVertex(
                    key_, outVertices[i], cost_ + shortcutCost);
        } else if (codec_.encode(outVertices[i]) == targetKey_) {
            relax(key_, codec_.encode(outVertices[i]), cost_ + shortcutCost);
        }
    }
}

template <class Policy>
boost::optional<std::pair<DijkstraKey, float>>
Dijkstra<Policy>::next() {
    if (const auto init = initialState()) {
        return init;
    }
    if (!codec_.isOverlayVertex(key_)) {
        relaxOriginalEdge(codec_.originalEdgeId(key_));
    } else {
        const auto overlayVertexId = codec_.overlayVertexId(key_);
        assert(overlay_->isInVertex(overlayVertexId));
        const auto componentVector =
            overlay_->overlayVertexComponentVector(overlayVertexId);
        if (Policy::level != LevelPolicy::Nonzero &&
                policy_.isZeroLevelComponent(componentVector)) {
            assert(policy_.isZeroLevelComponent(
                overlay_->vertexComponentVector(
                    graph_->edge(
                        overlay_->overlayVertexToOriginalEdge(
                            overlayVertexId)).target)));
            relax(
                    key_,
                    codec_.encode(
                        overlay_->overlayVertexToOriginalEdge(overlayVertexId)),
                    cost_);
        } else {
            relaxOverlayInVertex(overlayVertexId, componentVector);
        }
    }
    return popHeap();
}

template <class Policy>
ReverseDijkstra<Policy>::ReverseDijkstra(
        const Metric* metric,
        const Prepro* prepro,
        DijkstraStorage* storage,
        DijkstraKey sourceKey,
        DijkstraKey targetKey,
        Policy policy,
        std::function<void (DijkstraKey, float)> onRelax) :
    DijkstraBase(
            metric,
            prepro,
            storage,
            sourceKey,
            targetKey,
            targetKey,
            std::move(onRelax)),
    policy_(policy)
{
    storage_->states_[targetKey].cost = 0;
    storage_->black_.insert(targetKey);
}

template <class Policy>
void ReverseDijkstra<Policy>::relaxOriginalEdge(OriginalEdgeId edgeId) {
    const auto edgeSourceId = graph_->edge(edgeId).source;
    const auto componentVector =
        overlay_->vertexComponentVector(edgeSourceId);
    if (policy_.isZeroLevelComponent(componentVector)) {
        const auto& inEdges = graph_->inEdges(edgeSourceId);
        const size_t outEdgesSize = graph_->outEdges(edgeSourceId).size().untag();
        const float* const penalties = prepro_->turnTable(edgeSourceId) +
            graph_->outEdgeIndex(edgeId).untag();
        for (const auto i: range(inEdges.size())) {
            const float penalty = penalties[i.untag()*outEdgesSize];
            const Edge& inEdge = inEdges[i];
            relax(
                    key_,
                    codec_.encode(inEdge.id),
                    cost_ + penalty + metric_->cost(edgeId));
        }
    } else if (Policy::level != LevelPolicy::Zero) {
        relaxOverlayInVertex(
                codec_.encode(edgeId),
                overlay_->originalEdgeToOverlayInVertex(edgeId),
                cost_);
    }
}

template <class Policy>
void ReverseDijkstra<Policy>::relaxOverlayOutVertex(
        OverlayVertexId v, ComponentVector componentVector) {
    const auto levelSucc = policy_.componentLevel(componentVector);
    if (levelSucc == 0) {
        return;
    }
    const auto level = levelSucc.pred();
    const auto component = getComponentVector(level, componentVector);
    const auto& inVertices = overlay_->componentInVertices(level, component);
    const size_t outIndex = overlay_->outVertexIndex(level, component, v);
    const float* const costsBase =
        prepro_->overlayCosts(overlay_->costsOffset(level, component));
    const float* const costs = costsBase + outIndex;
    const size_t stride =
        overlay_->componentOutVertices(level, component).size();
    for (size_t i = 0; i != inVertices.size(); ++i) {
        const float shortcutCost = costs[i*stride];
        if (!policy_.isFrontier(inVertices[i])) {
            relaxOverlayInVertex(
                    key_, inVertices[i], cost_ + shortcutCost);
        } else if (codec_.encode(inVertices[i]) == sourceKey_) {
            relax(key_, codec_.encode(inVertices[i]), cost_ + shortcutCost);
        }
    }
}

template <class Policy>
void ReverseDijkstra<Policy>::relaxOverlayInVertex(
        DijkstraKey prev,
        OverlayVertexId v,
        float newCost) {
    assert(overlay_->isInVertex(v));
    const DijkstraKey key = codec_.encode(v);
    auto& state = storage_->states_[key];

    // This is 'relax' function hand-inlined here.
    if (newCost >= state.cost) {
        return;
    }
    state.cost = newCost;
    state.backlink = prev;
    const auto overlayEdge = overlay_->overlayOriginalEdge(v);
    onRelax_(key, newCost);
    relax(
        key,
        codec_.encode(overlayEdge.overlayVertexId),
        // TODO put original edges' cost into prepro.
        newCost + metric_->cost(overlayEdge.originalEdgeId));
}

template <class Policy>
boost::optional<std::pair<DijkstraKey, float>>
ReverseDijkstra<Policy>::next() {
    if (const auto init = initialState()) {
        return init;
    }
    if (!codec_.isOverlayVertex(key_)) {
        relaxOriginalEdge(codec_.originalEdgeId(key_));
    } else {
        const auto overlayVertexId = codec_.overlayVertexId(key_);
        assert(!overlay_->isInVertex(overlayVertexId));
        const auto componentVector =
            overlay_->overlayVertexComponentVector(overlayVertexId);
        if (Policy::level != LevelPolicy::Nonzero &&
                policy_.isZeroLevelComponent(componentVector)) {
            assert(!policy_.isZeroLevelComponent(
                overlay_->vertexComponentVector(
                    graph_->edge(
                        overlay_->overlayVertexToOriginalEdge(
                            overlayVertexId)).target)));
            const auto componentOutEdgeId =
                overlay_->overlayVertexToOriginalEdge(overlayVertexId);
            const auto edgeSourceId = graph_->edge(componentOutEdgeId).source;
            const float* const penalties = prepro_->turnTable(edgeSourceId) +
                graph_->outEdgeIndex(componentOutEdgeId).untag();
            const auto& inEdges = graph_->inEdges(edgeSourceId);
            const size_t stride = graph_->outEdges(edgeSourceId).size().untag();
            for (const auto i: range(inEdges.size())) {
                relax(
                        key_,
                        codec_.encode(inEdges[i].id),
                        cost_ + penalties[i.untag()*stride]);
            }
        } else {
            relaxOverlayOutVertex(overlayVertexId, componentVector);
        }
    }
    return popHeap();
}

template class Dijkstra<ShortestPathPolicy>;
template class Dijkstra<UnpackZeroLevelPolicy>;
template class Dijkstra<UnpackNonzeroLevelPolicy>;

template class ReverseDijkstra<ShortestPathPolicy>;
template class ReverseDijkstra<UnpackZeroLevelPolicy>;
template class ReverseDijkstra<UnpackNonzeroLevelPolicy>;

template std::vector<std::pair<LevelId, DijkstraKey>>
DijkstraStorage::backtrack<ShortestPathPolicy>(
        DijkstraKey sourceKey,
        DijkstraKey targetKey,
        ShortestPathPolicy policy) const;

template std::vector<std::pair<LevelId, DijkstraKey>>
DijkstraStorage::backtrack<UnpackZeroLevelPolicy>(
        DijkstraKey sourceKey,
        DijkstraKey targetKey,
        UnpackZeroLevelPolicy policy) const;

template std::vector<std::pair<LevelId, DijkstraKey>>
DijkstraStorage::backtrack<UnpackNonzeroLevelPolicy>(
        DijkstraKey sourceKey,
        DijkstraKey targetKey,
        UnpackNonzeroLevelPolicy policy) const;
