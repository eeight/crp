
#include "dense_hash_map.h"
#include "mt.h"
#include "prepro.h"
#include "range.h"
#include "tiny_graph.h"

#include <boost/progress.hpp>
#include <boost/format.hpp>

namespace {

struct TurnTableTag;
using TurnTableId = Tagged<TurnTableTag, uint32_t>;

} // namespace

class PreproBuilder {
public:
    PreproBuilder(
            const Graph* graph, const Overlay* overlay, const Metric* metric) :
        graph_(graph),
        overlay_(overlay),
        metric_(metric),
        tasks_(overlay->levelsNumber())
    {}

    PreproStorage<mms::Standalone> build()&& {
        prepro_.overlayCosts_.resize(overlay_->costsCount());
        size_t totalComponents = 0;
        for (auto level: range(overlay_->levelsNumber())) {
            totalComponents += overlay_->componentsAtLevel(level).untag();
        }
        boost::progress_display progress(totalComponents, std::cout, "");
        std::mutex mutex;

        for (auto level: range(overlay_->levelsNumber())) {
            for (auto component: range(overlay_->componentsAtLevel(level))) {
                tasks_[level].emplace_back(
                        level == 0 ?
                            0 :
                            overlay_->subcomponents(level, component).size(),
                        [=, &mutex, &progress] {
                            processOverlayComponent(level, component);
                            std::unique_lock<std::mutex> lock(mutex);
                            ++progress;
                        });
            }
        }

        for (auto level: range(overlay_->levelsNumber().pred())) {
            const auto level1 = level.succ();
            for (auto component: range(overlay_->componentsAtLevel(level1))) {
                for (const auto subcomponent:
                        overlay_->subcomponents(level1, component)) {
                    tasks_[level][subcomponent].setDependent(
                            &tasks_[level1][component]);
                }
            }
        }

        Dispatcher dispatcher;
        for (auto& task: tasks_.front()) {
            dispatcher.schedule(&task);
        }
        dispatcher.run();
        std::cout << '\n';

        buildTurnTables();

        return std::move(prepro_);
    }

private:
    void processOverlayComponent(LevelId level, GraphComponentId component) {
        TinyGraph tinyGraph;
        std::vector<TinyVertexId> inVertices;
        std::vector<TinyVertexId> outVertices;

        if (level == 0) {
            std::tie(tinyGraph, inVertices, outVertices) =
                cutoutLevel0Graph(component);
        } else {
            std::tie(tinyGraph, inVertices, outVertices) =
                cutoutLevelGraph(level, component);
        }
        float *outCosts = prepro_.overlayCosts_.data() +
            overlay_->costsOffset(level, component);
        for (size_t i = 0; i < inVertices.size(); i += 16) {
            const size_t iEnd = std::min(i + 16, inVertices.size());
            const auto costs =
                tinyGraph.bf(
                        {inVertices.begin() + i, inVertices.begin() + iEnd},
                        outVertices);
            std::copy(costs.begin(), costs.end(), outCosts);
            outCosts += costs.size();
        }
    }

    template <class F>
    void forallComponentOriginalEdges(GraphComponentId component, F f) const {
        for (const OriginalVertexId v:
                overlay_->componentOriginalVertices(component)) {
            for (const auto& edge: graph_->outEdges(v)) {
                if (overlay_->vertexComponent(0, edge.target) == component) {
                    f(edge);
                }
            }
        }
    }

    std::tuple<TinyGraph, std::vector<TinyVertexId>, std::vector<TinyVertexId>>
    cutoutLevel0Graph(GraphComponentId component) {
        DenseHashMap<OriginalEdgeId, TinyVertexId> tinyVertices;
        DenseHashMap<OverlayVertexId, TinyVertexId> overlayTinyVertices;
        TinyGraphBuilder builder;
        std::vector<TinyVertexId> inVertices;
        std::vector<TinyVertexId> outVertices;

        // Add original edges.
        forallComponentOriginalEdges(
            component, [&tinyVertices, &builder](const Edge& edge) {
                tinyVertices.emplace(edge.id, builder.addVertex());
            });
        // Add overlay vertices.
        for (const OverlayVertexId inVertex:
                overlay_->componentInVertices(0, component)) {
            const TinyVertexId v = builder.addVertex();
            overlayTinyVertices.emplace(inVertex, v);
            inVertices.push_back(v);
        }
        for (const OverlayVertexId outVertex:
                overlay_->componentOutVertices(0, component)) {
            const TinyVertexId v = builder.addVertex();
            overlayTinyVertices.emplace(outVertex, v);
            outVertices.push_back(v);
        }

        auto targetVertex = [&, this](const Edge& edge) {
            return overlay_->vertexComponent(0, edge.target) == component ?
                tinyVertices.at(edge.id) :
                overlayTinyVertices.at(
                        overlay_->originalEdgeToOverlayOutVertex(edge.id));
        };

        forallComponentOriginalEdges(
            component,
            [
                this,
                component,
                &builder,
                &tinyVertices,
                &overlayTinyVertices,
                &targetVertex
            ](const Edge& edge) {
                const float edgeCost = metric_->cost(edge.id);
                for (const Edge& nextEdge: graph_->outEdges(edge.target)) {
                    builder.addEdge(
                        tinyVertices.at(edge.id),
                        targetVertex(nextEdge),
                        edgeCost + metric_->turnCost(edge.id, nextEdge.id));
                }
            });

        for (const OverlayVertexId inVertex:
                overlay_->componentInVertices(0, component)) {
            const auto& edge = graph_->edge(
                    overlay_->overlayVertexToOriginalEdge(inVertex));
            for (const Edge& nextEdge: graph_->outEdges(edge.target)) {
                builder.addEdge(
                    overlayTinyVertices.at(inVertex),
                    targetVertex(nextEdge),
                    metric_->turnCost(edge.id, nextEdge.id));
            }
        }

        return std::make_tuple(builder.build(), inVertices, outVertices);
    }

    std::tuple<TinyGraph, std::vector<TinyVertexId>, std::vector<TinyVertexId>>
    cutoutLevelGraph(LevelId level, GraphComponentId component) {
        TinyGraphBuilder builder;
        DenseHashMap<OverlayVertexId, TinyVertexId> tinyVertices;

        // Take all overlay vertices from enclosed coponents at the lower level.
        for (GraphComponentId subcomponent:
                overlay_->subcomponents(level, component)) {
            for (const OverlayVertexId v:
                    overlay_->componentInVertices(level.pred(), subcomponent)) {
                tinyVertices.emplace(v, builder.addVertex());
            }
            for (const OverlayVertexId v:
                    overlay_->componentOutVertices(level.pred(), subcomponent)) {
                tinyVertices.emplace(v, builder.addVertex());
            }
        }

        // Draw full bipartite graphs.
        for (GraphComponentId subcomponent:
                overlay_->subcomponents(level, component)) {
            const float *cost = prepro_.overlayCosts_.data() +
                overlay_->costsOffset(level.pred(), subcomponent);
            for (const OverlayVertexId v:
                    overlay_->componentInVertices(level.pred(), subcomponent)) {
                for (const OverlayVertexId u:
                        overlay_->componentOutVertices(level.pred(), subcomponent)) {
                    builder.addEdge(
                            tinyVertices.at(v), tinyVertices.at(u), *cost++);
                }
            }
        }

        // Connect them using original edges.
        for (GraphComponentId subcomponent:
                overlay_->subcomponents(level, component)) {
            for (const OverlayVertexId v:
                    overlay_->componentOutVertices(level.pred(), subcomponent)) {
                const OverlayOriginalEdge& overlayEdge =
                    overlay_->overlayOriginalEdge(v);
                if (tinyVertices.count(overlayEdge.overlayVertexId)) {
                    builder.addEdge(
                            tinyVertices.at(v),
                            tinyVertices.at(overlayEdge.overlayVertexId),
                            metric_->cost(overlayEdge.originalEdgeId));
                }
            }
        }

        std::vector<TinyVertexId> inVertices;
        for (const OverlayVertexId v:
                overlay_->componentInVertices(level, component)) {
            inVertices.push_back(tinyVertices.at(v));
        }
        std::vector<TinyVertexId> outVertices;
        for (const OverlayVertexId v:
                overlay_->componentOutVertices(level, component)) {
            outVertices.push_back(tinyVertices.at(v));
        }

        return std::make_tuple(builder.build(), inVertices, outVertices);
    }

    void buildTurnTables() {
        std::cout << "Building turn tables...\n";
        boost::progress_display progress(
                graph_->verticesNumber().untag(),
                std::cout,
                "");
        prepro_.turnTableOffsets_.resize(graph_->verticesNumber());
        ThreadPool pool(std::thread::hardware_concurrency());
        const OriginalVertexId BATCH_SIZE(10000);
        for (OriginalVertexId begin = 0; begin < graph_->verticesNumber();
                begin += BATCH_SIZE) {
            const auto end = std::min(
                    graph_->verticesNumber(), begin + BATCH_SIZE);
            pool.push([this, begin, end, &progress] {
                processTurnTables(begin, end);
                std::lock_guard<std::mutex> lock(turnTablesMutex_);
                progress += (end - begin).untag();
            });
        }
        pool.finalize();
    }

    void processTurnTables(OriginalVertexId begin, OriginalVertexId end) {
        std::map<std::vector<float>, TurnTableId> map;
        TaggedVector<OriginalVertexTag, TurnTableId> turnTableIds(
                end - begin, 0);
        std::vector<float> nextTable;
        TurnTableId nextId = 0;
        for (const auto v: range(begin, end)) {
            nextTable.clear();
            for (const auto& inEdge: graph_->inEdges(v)) {
                for (const auto& outEdge: graph_->outEdges(v)) {
                    nextTable.push_back(
                            metric_->turnCost(inEdge.id, outEdge.id));
                }
            }
            auto iter = map.find(nextTable);
            if (iter == map.end()) {
                iter = map.emplace(std::move(nextTable), nextId++).first;
            }
            turnTableIds[v - begin] = iter->second;
        }

        std::unordered_map<TurnTableId, size_t> offsetsMap;

        {
            std::lock_guard<std::mutex> lock(turnTablesMutex_);
            for (const auto& kv: map) {
                const auto& table = kv.first;
                const auto turnTableId = kv.second;
                auto iter = turnTablesOffsets_.find(table);
                if (iter == turnTablesOffsets_.end()) {
                    const size_t nextOffset = prepro_.turnTablesCosts_.size();
                    prepro_.turnTablesCosts_.insert(
                        prepro_.turnTablesCosts_.end(),
                        table.begin(),
                        table.end());
                    iter = turnTablesOffsets_.emplace(
                            std::move(table), nextOffset).first;
                }
                offsetsMap[turnTableId] = iter->second;
            }
        }

        for (const auto v: range(begin, end)) {
            prepro_.turnTableOffsets_[v] = offsetsMap.at(turnTableIds[v - begin]);
        }
    }

    const Graph* graph_;
    const Overlay* overlay_;
    const Metric* metric_;
    PreproStorage<mms::Standalone> prepro_;
    TaggedVector<LevelTag, TaggedVector<GraphComponentTag, Task>> tasks_;
    std::mutex turnTablesMutex_;
    std::map<std::vector<float>, size_t> turnTablesOffsets_;
};

PreproStorage<mms::Standalone> buildPrepro(
        const Graph& graph, const Overlay& overlay, const Metric& metric)
{
    return PreproBuilder(&graph, &overlay, &metric).build();
}
