#pragma once

#include "tagged.h"

#include <iostream>

template <class Tag, class T>
std::ostream& operator <<(std::ostream& out, Tagged<Tag, T> t) {
    return out << t.untag();
}
