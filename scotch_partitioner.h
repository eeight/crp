#pragma once

#include "partition.h"

OneLevelPartition scotch(const Skeleton& skeleton, size_t partsNumber);
