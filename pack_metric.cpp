#include "mmap.h"
#include "stored_graph.h"
#include "stored_metric.h"

#include <mms/cast.h>
#include <mms/writer.h>

#include <cstdio>
#include <fstream>

int main() {
    StoredGraph graph("genfiles/graph.mms");
    MetricStorageBuilder builder(&graph);
    int u, v;
    float cost;
    while (scanf("%d %d %f", &u, &v, &cost) == 3) {
        builder.addCost(OriginalVertexId(u), OriginalVertexId(v), cost);
    }
    std::ofstream out("genfiles/graph_metric.mms");
    mms::safeWrite(out, std::move(builder).build());
    out.close();
    return 0;
}
