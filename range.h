#pragma once

#include "tagged.h"

#include <boost/range/counting_range.hpp>
#include <boost/range/adaptor/transformed.hpp>

template <class Tag, class T>
boost::transformed_range<
    Tagged<Tag, T> (*)(T),
    boost::iterator_range<boost::counting_iterator<T>>>
range(Tagged<Tag, T> min, Tagged<Tag, T> max) {
    auto untaggedRange = boost::counting_range<T>(min.untag(), max.untag());
    return {&mkTagged<Tag, T>, untaggedRange};
}

template <class Tag, class T>
boost::transformed_range<
    Tagged<Tag, T> (*)(T),
    boost::iterator_range<boost::counting_iterator<T>>>
range(Tagged<Tag, T> max) {
    return range(Tagged<Tag, T>(static_cast<T>(0)), max);
}

