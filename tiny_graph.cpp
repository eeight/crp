#include "tiny_graph.h"

#include <algorithm>
#include <numeric>
#include <vector>

#include <xmmintrin.h>

namespace {

class Set {
public:
    explicit Set(size_t verticesNumber) :
            have_(verticesNumber, false) {
        items_.reserve(verticesNumber);
    }

    void add(TinyVertexId v) {
        if (!have_[v.untag()]) {
            have_[v.untag()] = true;
            items_.push_back(v);
        }
    }

    bool has(TinyVertexId v) const {
        return have_[v.untag()];
    }

    const std::vector<TinyVertexId>& items() const {
        return items_;
    }

    bool empty() const {
        return items_.empty();
    }

    void swap(Set& other) {
        std::swap(have_, other.have_);
        std::swap(items_, other.items_);
    }

    void reset() {
        for (const auto v: items_) {
            have_[v.untag()] = false;
        }
        items_.clear();
    }
private:
    std::vector<bool> have_;
    std::vector<TinyVertexId> items_;
};

} // namespace

std::vector<float> TinyGraph::bf(
        const std::vector<TinyVertexId>& inVertices,
        const std::vector<TinyVertexId>& outVertices) const {
    Set active(verticesNumber_.untag());
    Set nextActive(verticesNumber_.untag());

    // Make sure that stride is divisible by 4 otherwise
    // weight strands would not be aligned to 16 bytes.
    const size_t weightStride = inVertices.size() % 4 == 0 ?
        inVertices.size() : inVertices.size() + 4 - (inVertices.size() % 4);

    std::vector<float> weights(
            verticesNumber_.untag()*weightStride,
            std::numeric_limits<float>::infinity());
    for (size_t i = 0; i != inVertices.size(); ++i) {
        active.add(inVertices[i]);
        weights[inVertices[i].untag()*weightStride + i] = 0;
    }

    for (; !active.empty(); active.swap(nextActive)) {
        nextActive.reset();
        assert(nextActive.empty());
        for (const auto u: active.items()) {
            for (size_t offset = outEdgesOffset_[u.untag()],
                    offsetEnd = outEdgesOffset_[u.untag() + 1];
                    offset != offsetEnd;
                    ++offset) {
                const TinyVertexId v = outEdges_[offset];
                const float w = outWeights_[offset];

                float* const sourceWeightsStrand =
                    weights.data() + u.untag()*weightStride;
                float* const targetWeightsStrand =
                    weights.data() + v.untag()*weightStride;

                size_t i = 0;
                const __m128 mmW = _mm_load_ps1(&w);
                for (; i + 4 <= inVertices.size(); i += 4) {
                    const __m128 nextWeights =
                        _mm_load_ps(sourceWeightsStrand + i) + mmW;
                    const __m128 targetWeights = _mm_load_ps(
                            targetWeightsStrand + i);
                    // See if any of the current weights is greater than
                    // the we have through current edge.
                    const __m128 gt = _mm_cmpgt_ps(targetWeights, nextWeights);
                    if (_mm_movemask_epi8(_mm_castps_si128(gt))) {
                        // Store uppated weights.
                        _mm_store_ps(
                                targetWeightsStrand + i, _mm_min_ps(targetWeights, nextWeights));
                        if (!active.has(v)) {
                            nextActive.add(v);
                        }
                    }
                }
                for (; i != inVertices.size(); ++i) {
                    const float nextWeight = sourceWeightsStrand[i] + w;
                    if (targetWeightsStrand[i] > nextWeight) {
                        targetWeightsStrand[i] = nextWeight;
                        nextActive.add(v);
                    }
                }
            }
        }
    }

    std::vector<float> result(inVertices.size()*outVertices.size());
    for (size_t i = 0; i != inVertices.size(); ++i) {
        for (size_t j = 0; j != outVertices.size(); ++j) {
            result[i*outVertices.size() + j] = weights[
                outVertices[j].untag()*weightStride + i];
        }
    }

    return result;
}

TinyVertexId TinyGraphBuilder::addVertex() {
    return verticesNumber_++;
}

void TinyGraphBuilder::addEdge(
        TinyVertexId source, TinyVertexId target, float weight) {
    edges_.push_back({source, target, weight});
}

TinyGraph TinyGraphBuilder::build() {
    std::sort(edges_.begin(), edges_.end());
    TinyGraph g;

    g.verticesNumber_ = verticesNumber_;
    g.outEdgesOffset_.resize(verticesNumber_.untag() + 1);
    g.outEdges_.resize(edges_.size(), 0);
    g.outWeights_.resize(edges_.size());
    for (size_t i = 0; i != edges_.size(); ++i) {
        g.outEdges_[i] = edges_[i].target;
        g.outWeights_[i] = edges_[i].weight;
        ++g.outEdgesOffset_[edges_[i].source.untag() + 1];
    }
    std::partial_sum(
        g.outEdgesOffset_.begin(),
        g.outEdgesOffset_.end(),
        g.outEdgesOffset_.begin());

    return g;
}
