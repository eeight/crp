#pragma once

#include "graph.h"
#include "printer.h"
#include "printer.h"

#include <mms/vector.h>
#include <mms/map.h>

#include <algorithm>
#include <cstdint>
#include <map>
#include <vector>

struct OverlayVertexTag;
struct GraphComponentTag;
struct LevelTag;
struct ComponentVectorTag;

using OverlayVertexId = Tagged<OverlayVertexTag, uint32_t>;
using GraphComponentId = Tagged<GraphComponentTag, uint32_t>;
using LevelId = Tagged<LevelTag, uint8_t>;
using ComponentVector = Tagged<ComponentVectorTag, uint64_t>;

GraphComponentId getComponentVector(LevelId level, ComponentVector vector);
LevelId commonLevel(ComponentVector a, ComponentVector b);

// NOTE [Overlay vertices numbering]
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// 1. First all out-vertices, then all in-vertices.
// 2. First vertices at higher levels, then vertices at lower levels
// 3. Otherwise order is the same as in the original graph.

struct OverlayOriginalEdge {
    OverlayVertexId overlayVertexId;
    OriginalEdgeId originalEdgeId;
};

template <class P>
class OverlayStorage {
public:
    template <class A>
    void traverseFields(A&& a) const {
        a(
            vertexComponents_,
            overlayComponents_,
            componentOriginalVertices_,
            overlayOriginalEdges_,
            componentOutVertices_,
            componentInVertices_,
            subcomponents_,
            costsOffset_,
            costsCount_,
            originalEdgeToOverlayOutVertex_,
            originalEdgeToOverlayInVertex_,
            overlayVertexToOriginalEdge_);
    }

    LevelId levelsNumber() const {
        return LevelId(componentInVertices_.size());
    }

    GraphComponentId vertexComponent(LevelId level, OriginalVertexId v) const {
        return getComponentVector(level, vertexComponents_[v]);
    }

    ComponentVector vertexComponentVector(OriginalVertexId v) const {
        return vertexComponents_[v];
    }

    GraphComponentId overlayVertexComponent(
            LevelId level, OverlayVertexId v) const {
        return getComponentVector(level, overlayComponents_[v]);
    }

    ComponentVector overlayVertexComponentVector(OverlayVertexId v) const {
        return overlayComponents_[v];
    }

    OverlayVertexId overlayVerticesNumber() const {
        return overlayComponents_.size();
    }

    GraphComponentId componentsAtLevel(LevelId level) const {
        return GraphComponentId(componentInVertices_[level].size());
    }

    size_t costsOffset(LevelId level, GraphComponentId component) const {
        return costsOffset_[level][component];
    }

    uint32_t costsCount() const {
        return costsCount_;
    }

    const mms::vector<P, OriginalVertexId>& componentOriginalVertices(
            GraphComponentId component) const {
        return componentOriginalVertices_[component];
    }

    OverlayOriginalEdge overlayOriginalEdge(OverlayVertexId v) const {
        return overlayOriginalEdges_.at(v);
    }

    bool isInVertex(OverlayVertexId v) const {
        return !isOutVertex(v);
    }

    bool isOutVertex(OverlayVertexId v) const {
        return v*2 < overlayOriginalEdges_.size();
    }

    size_t inVertexIndex(
            LevelId level, GraphComponentId component, OverlayVertexId v) const {
        assert(isInVertex(v));
        const auto& vertices = componentInVertices(level, component);
        auto iter = std::lower_bound(vertices.begin(), vertices.end(), v);
        if (iter != vertices.end()) {
            assert(*iter == v);
            return iter - vertices.begin();
        } else {
            throw std::logic_error(
                    "inVertexIndex: given vertex is not an in-vertex at all");
        }
    }

    size_t outVertexIndex(
            LevelId level, GraphComponentId component, OverlayVertexId v) const {
        const auto& vertices = componentOutVertices(level, component);
        auto iter = std::lower_bound(vertices.begin(), vertices.end(), v);
        if (iter != vertices.end()) {
            assert(*iter == v);
            return iter - vertices.begin();
        } else {
            throw std::logic_error(
                    "outVertexIndex: given vertex is not an out-vertex at all");
        }
    }

    const mms::vector<P, OverlayVertexId> &componentOutVertices(
            LevelId level, GraphComponentId component) const {
        return componentOutVertices_[level][component];
    }

    const mms::vector<P, OverlayVertexId> &componentInVertices(
            LevelId level, GraphComponentId component) const {
        return componentInVertices_[level][component];
    }

    const mms::vector<P, GraphComponentId>& subcomponents(
            LevelId level, GraphComponentId component) const {
        return subcomponents_[level][component];
    }

    OverlayVertexId originalEdgeToOverlayOutVertex(
            OriginalEdgeId edgeId) const {
        return originalEdgeToOverlayOutVertex_.at(edgeId);
    }

    OverlayVertexId originalEdgeToOverlayInVertex(
            OriginalEdgeId edgeId) const {
        return originalEdgeToOverlayInVertex_.at(edgeId);
    }

    OriginalEdgeId overlayVertexToOriginalEdge(OverlayVertexId v) const {
        return overlayVertexToOriginalEdge_.at(
            isOutVertex(v) ? v : v - overlayOriginalEdges_.size()/2);
    }

    void print(Printer* printer) const {
        printer->begin("overlay");
#define DO(x) printer->key(#x); printer->value(x)
        DO(vertexComponents_);
        DO(overlayComponents_);
        DO(componentOriginalVertices_);
        printer->key("overlayOriginalEdges_");
        printer->begin("vector");
        for (const auto v: range(overlayOriginalEdges_.size())) {
            printer->key(v);
            printer->begin("OverlayOriginalEdge");
            printer->key("overlayVertex");
            printer->value(overlayOriginalEdges_[v].overlayVertexId);
            printer->key("originalEdge");
            printer->value(overlayOriginalEdges_[v].originalEdgeId);
            printer->end();
        }
        printer->end();
        DO(componentOutVertices_);
        DO(componentInVertices_);
        DO(subcomponents_);
        DO(costsOffset_);
        DO(costsCount_);
        DO(originalEdgeToOverlayOutVertex_);
        DO(originalEdgeToOverlayInVertex_);
        DO(overlayVertexToOriginalEdge_);
#undef DO
        printer->end();

    }

private:
    // Original vertex -> (level -> Component)
    TaggedMmsVector<P, OriginalVertexTag, ComponentVector> vertexComponents_;

    // Overlay vertex -> (level -> Component)
    TaggedMmsVector<P, OverlayVertexTag, ComponentVector> overlayComponents_;

    // All original vertices in the given component.
    TaggedMmsVector<P, GraphComponentTag, mms::vector<P, OriginalVertexId>>
            componentOriginalVertices_;

    // Overlay vertex -> original edge that comes in or out of it.
    TaggedMmsVector<P, OverlayVertexTag, OverlayOriginalEdge>
        overlayOriginalEdges_;

    TaggedMmsVector<P, LevelTag,
        TaggedMmsVector<P, GraphComponentTag,
            mms::vector<P, OverlayVertexId>>>
                componentOutVertices_;

    TaggedMmsVector<P, LevelTag,
        TaggedMmsVector<P, GraphComponentTag,
            mms::vector<P, OverlayVertexId>>>
                componentInVertices_;

    TaggedMmsVector<P, LevelTag,
        TaggedMmsVector<P, GraphComponentTag,
            mms::vector<P, GraphComponentId>>>
                subcomponents_;

    // component number -> costs offset.
    TaggedMmsVector<P, LevelTag,
        TaggedMmsVector<P, GraphComponentTag, uint32_t>> costsOffset_;

    uint32_t costsCount_;

    mms::map<P, OriginalEdgeId, OverlayVertexId>
        originalEdgeToOverlayOutVertex_;

    mms::map<P, OriginalEdgeId, OverlayVertexId>
        originalEdgeToOverlayInVertex_;

    TaggedMmsVector<P, OverlayVertexTag, OriginalEdgeId>
        overlayVertexToOriginalEdge_;

    friend class OverlayBuilder;
};

namespace std {

template <>
struct is_trivial<OverlayOriginalEdge> : integral_constant<bool, true> {};

} // namespace std

using OneLevelPartition = TaggedVector<OriginalVertexTag, GraphComponentId>;
using Partitions = TaggedVector<LevelTag, OneLevelPartition>;
using Overlay = OverlayStorage<mms::Mmapped>;
using StandaloneOverlay = OverlayStorage<mms::Standalone>;

StandaloneOverlay buildOverlay(
        const Graph& graph, const Partitions& partitions);
