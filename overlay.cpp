#include "overlay.h"
#include "range.h"

#include <algorithm>
#include <array>

#include <boost/optional.hpp>

namespace {

const std::array<uint64_t, 6> BITS_AT_LEVEL = {{16, 14, 12, 10, 8, 4}};
const std::array<uint64_t, 6> CUMULATIVE_BITS_AT_LEVEL = {{0, 16, 30, 42, 52, 60}};

uint64_t levelMask(LevelId level) {
    return ((1ull << BITS_AT_LEVEL[level.untag()]) - 1) <<
        CUMULATIVE_BITS_AT_LEVEL[level.untag()];
}

void setComponentVector(
        LevelId level, GraphComponentId component, ComponentVector* vector) {
    assert(level.untag() < BITS_AT_LEVEL.size());
    assert(component.untag() < (1ull << BITS_AT_LEVEL[level.untag()]));

    uint64_t value = vector->untag();
    value &= ~levelMask(level);
    value |= (static_cast<uint64_t>(component.untag()) <<
        CUMULATIVE_BITS_AT_LEVEL[level.untag()]);

    *vector = ComponentVector(value);
}

} // namespace

LevelId commonLevel(ComponentVector a, ComponentVector b) {
    if (a == b) {
        return 0;
    } else {
        const size_t index = 64 - __builtin_clzll(a.untag() ^ b.untag());
        // TODO Precompute lookup for this.
        for (size_t i = 0; i + 1 != BITS_AT_LEVEL.size(); ++i) {
            if (index < BITS_AT_LEVEL[i] + CUMULATIVE_BITS_AT_LEVEL[i]) {
                return LevelId(i + 1);
            }
        }
    }

    return LevelId(BITS_AT_LEVEL.size());
}

GraphComponentId getComponentVector(LevelId level, ComponentVector vector) {
    return GraphComponentId(
            (vector.untag() & levelMask(level)) >>
            CUMULATIVE_BITS_AT_LEVEL[level.untag()]);
}

namespace {

struct OverlayEdgeDesc {
    LevelId level;
    OriginalEdgeId edgeId;

    bool operator <(const OverlayEdgeDesc& other) const {
        if (level != other.level) {
            return level > other.level;
        } else {
            return edgeId < other.edgeId;
        }
    }
};

} // namespace

class OverlayBuilder {
public:
    OverlayBuilder(const Graph* graph, const Partitions* partitions) :
        graph_(graph),
        partitions_(partitions)
    {}

    StandaloneOverlay build()&&
    {
        makeVertexComponents();
        makeComponentOriginalVertices();
        makeOverlayEdges();
        makeOverlayVertices();
        makeOverlayOriginalEdges();
        makeOverlayComponents();
        makeComponentInOutVertices();
        makeSubcomponents();
        makeCostsOffset();
        makeOverlayToOriginal();
        return std::move(overlay_);
    }

private:
    void makeVertexComponents() {
        overlay_.vertexComponents_.resize(graph_->verticesNumber(), 0);
        for (auto v: range(graph_->verticesNumber())) {
            for (auto l: range(levelsNumber())) {
                setComponentVector(
                        l, partition(l, v), &overlay_.vertexComponents_[v]);
            }
        }
    }

    void makeComponentOriginalVertices() {
        overlay_.componentOriginalVertices_.resize(partitionsAtLevel(0));
        for (auto v: range(graph_->verticesNumber())) {
            overlay_.componentOriginalVertices_[partition(0, v)].push_back(v);
        }
    }

    void makeOverlayEdges() {
        for (const auto& e: graph_->edges()) {
            const auto level = commonLevel(e.source, e.target);
            if (level != 0) {
                overlayEdges_.push_back({level, e.id});
            }
        }
        std::sort(overlayEdges_.begin(), overlayEdges_.end());
    }

    void makeOverlayVertices() {
        for (auto v: range(overlayEdges_.size())) {
            overlay_.originalEdgeToOverlayOutVertex_.emplace(
                    overlayEdges_[v].edgeId, v);
            overlay_.originalEdgeToOverlayInVertex_.emplace(
                    overlayEdges_[v].edgeId,
                    v + overlayEdges_.size());
        }
    }

    void makeOverlayOriginalEdges() {
        overlay_.overlayOriginalEdges_.resize(
                overlayEdges_.size()*2, OverlayOriginalEdge{0, 0});
        for (const auto v: range(overlayEdges_.size())) {
            const OriginalEdgeId edgeId = overlayEdges_[v].edgeId;
            overlay_.overlayOriginalEdges_.at(v) =
                {overlay_.originalEdgeToOverlayInVertex_.at(edgeId), edgeId};
            overlay_.overlayOriginalEdges_.at(v + overlayEdges_.size()) =
                {overlay_.originalEdgeToOverlayOutVertex_.at(edgeId), edgeId};
        }
    }

    void makeOverlayComponents() {
        overlay_.overlayComponents_.resize(overlayEdges_.size()*2, 0);
        for (const auto& kv: overlay_.originalEdgeToOverlayInVertex_) {
            overlay_.overlayComponents_.at(kv.second) =
                overlay_.vertexComponentVector(graph_->edge(kv.first).target);
        }
        for (const auto& kv: overlay_.originalEdgeToOverlayOutVertex_) {
            overlay_.overlayComponents_.at(kv.second) =
                overlay_.vertexComponentVector(graph_->edge(kv.first).source);
        }
    }

    void makeComponentInOutVertices() {
        overlay_.componentInVertices_.resize(levelsNumber());
        overlay_.componentOutVertices_.resize(levelsNumber());
        for (auto level: range(levelsNumber())) {
            overlay_.componentInVertices_[level].resize(
                    partitionsAtLevel(level));
            overlay_.componentOutVertices_[level].resize(
                    partitionsAtLevel(level));
        }
        for (const Edge& edge: graph_->edges()) {
            for (auto level: range(levelsNumber())) {
                const auto sourceComponent = partition(level, edge.source);
                const auto targetComponent = partition(level, edge.target);
                if (sourceComponent == targetComponent) {
                    break;
                }

                overlay_.componentOutVertices_[level][sourceComponent]
                    .push_back(overlay_.originalEdgeToOverlayOutVertex_.at(
                            edge.id));
                overlay_.componentInVertices_[level][targetComponent]
                    .push_back(overlay_.originalEdgeToOverlayInVertex_.at(
                            edge.id));
            }
        }
        for (auto level: range(levelsNumber())) {
            for (auto& vs: overlay_.componentOutVertices_[level]) {
                std::sort(vs.begin(), vs.end());
            }
            for (auto& vs: overlay_.componentInVertices_[level]) {
                std::sort(vs.begin(), vs.end());
            }
        }
    }

    void makeSubcomponents() {
        // Make sure that partition is consistent.
        TaggedVector<
            GraphComponentTag,
            boost::optional<ComponentVector>> componentVectors(
                partitionsAtLevel(0));

        for (const auto v: range(graph_->verticesNumber())) {
            const auto c = overlay_.vertexComponent(0, v);
            const auto cv = overlay_.vertexComponentVector(v);
            if (!componentVectors.at(c)) {
                componentVectors[c] = cv;
            } else {
                if (*componentVectors.at(c) != cv) {
                    throw std::logic_error(
                            "buildOverlay: partition is inconsistent");
                }
            }
        }

        overlay_.subcomponents_.resize(levelsNumber());
        for (const LevelId level: range(levelsNumber().pred())) {
            const LevelId nextLevel = level.succ();
            TaggedVector<
                GraphComponentTag,
                std::unordered_set<GraphComponentId>> subcomponents(
                        partitionsAtLevel(nextLevel));
            for (const auto cv: componentVectors) {
                const auto subcomponent = getComponentVector(level, *cv);
                const auto component = getComponentVector(nextLevel, *cv);
                subcomponents[component].insert(subcomponent);
            }
            overlay_.subcomponents_[nextLevel].resize(subcomponents.size());
            for (const auto component: range(subcomponents.size())) {
                overlay_.subcomponents_[nextLevel][component].assign(
                        subcomponents[component].begin(), subcomponents[component].end());
            }
        }
    }

    void makeCostsOffset() {
        overlay_.costsOffset_.resize(levelsNumber());

        uint32_t offset = 0;
        for (auto level: range(levelsNumber())) {
            for (auto component:
                    range(overlay_.componentOutVertices_[level].size())) {
                overlay_.costsOffset_[level].push_back(offset);
                offset +=
                    overlay_.componentOutVertices_[level][component].size()*
                    overlay_.componentInVertices_[level][component].size();
            }
        }
        overlay_.costsCount_ = offset;
    }

    void makeOverlayToOriginal() {
        overlay_.overlayVertexToOriginalEdge_.resize(overlayEdges_.size(), 0);
        for (const auto kv: overlay_.originalEdgeToOverlayOutVertex_) {
            overlay_.overlayVertexToOriginalEdge_.at(kv.second) = kv.first;
        }
    }

    GraphComponentId partitionsAtLevel(LevelId level) const {
        return std::max_element(
            partitions_->at(level).begin(),
            partitions_->at(level).end())->succ();
    }

    GraphComponentId partition(LevelId level, OriginalVertexId v) {
        if (level.succ() == partitions_->size()) {
            return 0;
        } else {
            return partitions_->at(level).at(v);
        }
    }

    LevelId levelsNumber() const {
        return partitions_->size();
    }

    LevelId commonLevel(OriginalVertexId v, OriginalVertexId u) const {
        return ::commonLevel(
                overlay_.vertexComponentVector(v),
                overlay_.vertexComponentVector(u));
    }

    const Graph* graph_;
    const Partitions* partitions_;
    TaggedVector<OverlayVertexTag, OverlayEdgeDesc> overlayEdges_;
    StandaloneOverlay overlay_;
};

StandaloneOverlay buildOverlay(
        const Graph& graph, const Partitions& partitions) {
    return OverlayBuilder(&graph, &partitions).build();
}
