#pragma once

#include <atomic>
#include <functional>
#include <mutex>
#include <condition_variable>
#include <stack>
#include <thread>
#include <vector>

class Task {
public:
    Task(size_t prereqs, std::function<void ()> task) :
        prereqs_(prereqs), task_(std::move(task))
    {}

    Task(const Task& other) :
        prereqs_(other.prereqs_.load()),
        task_(other.task_),
        dependent_(other.dependent_)
    {}

    void setDependent(Task *dep) {
        dependent_ = dep;
    }

    Task* dependent() const { return dependent_; }

    bool prereqDone() {
        return --prereqs_ == 0;
    }

    bool runnable() {
        return prereqs_.load() == 0;
    }

    void run() {
        task_();
    }

private:
    std::atomic<size_t> prereqs_;
    std::function<void ()> task_;
    Task* dependent_ = nullptr;
};

class ThreadPool {
public:
    explicit ThreadPool(size_t threads);

    void push(std::function<void ()> task);

    void finalize();

private:
    void worker();

    std::mutex mutex_;
    std::condition_variable haveWork_;
    bool stopping_ = false;
    std::stack<std::function<void ()>> stack_;

    std::vector<std::thread> threads_;
};

class Dispatcher {
public:
    Dispatcher();
    void schedule(Task* task);
    void run();

private:
    void doSchedule(Task* task);

    ThreadPool pool_;
};
