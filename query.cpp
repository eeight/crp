#include "query.h"
#include "heap.h"
#include "path_cache.h"
#include "dijkstra.h"

#include <boost/next_prior.hpp>

#include <map>
#include <set>
#include <limits>

namespace {

template <class Overlay>
ShortestPathPolicy makeShortestPathPolicy(
        const Graph& graph,
        const Overlay& overlay,
        OriginalEdgeId source,
        OriginalEdgeId target)  {
    return {
        overlay.vertexComponentVector(graph.edge(source).target),
        overlay.vertexComponentVector(graph.edge(target).target)
    };
}


} // namespace

Query::Query(const Graph* graph, const Overlay* overlay) :
    codec_(overlay->overlayVerticesNumber(), graph->edgesNumber()),
    storage_(graph, overlay),
    reverseStorage_(graph, overlay)
{}

template <class Policy>
std::pair<float, std::vector<std::pair<LevelId, DijkstraKey>>> Query::query(
        const Metric& metric,
        const Prepro& prepro,
        DijkstraKey sourceKey,
        DijkstraKey targetKey,
        Policy forwardPolicy,
        Policy reversePolicy) {
    float bestCost = std::numeric_limits<float>::infinity();
    boost::optional<DijkstraKey> bestMeetingPoint;

    auto tryImprove = [&](DijkstraKey key, float cost, auto& storage) {
        if (!storage.states_.count(key)) {
            return;
        }
        const float otherCost = storage.states_.at(key).cost;
        const float newEstimate = cost + otherCost;
        if (newEstimate < bestCost) {
            bestCost = newEstimate;
            bestMeetingPoint = key;
        }
    };

    Dijkstra<Policy> dijkstra(
            &metric,
            &prepro,
            &storage_,
            sourceKey,
            targetKey,
            forwardPolicy,
            [&, this](DijkstraKey key, float cost) {
                tryImprove(key, cost, reverseStorage_);
            });

    ReverseDijkstra<Policy> reverseDijkstra(
            &metric,
            &prepro,
            &reverseStorage_,
            sourceKey,
            targetKey,
            reversePolicy,
            [&, this](DijkstraKey key, float cost) {
                tryImprove(key, cost, storage_);
            });

    for (;;) {
        const auto forwardStep = dijkstra.next();
        if (!forwardStep) {
            break;
        }
        const auto reverseStep = reverseDijkstra.next();
        if (!reverseStep) {
            break;
        }

        if (forwardStep->first == targetKey ||
                reverseStep->first == sourceKey) {
            break;
        }

        if (forwardStep->second + reverseStep->second >= bestCost) {
            break;
        }
    }

    if (!bestMeetingPoint) {
        return {bestCost, {}};
    } else {
        auto path = storage_.backtrack(sourceKey, *bestMeetingPoint, forwardPolicy);
        std::reverse(path.begin(), path.end());
        const auto& reversePath =
            reverseStorage_.backtrack(targetKey, *bestMeetingPoint, reversePolicy);
        assert(!reversePath.empty());
        path.insert(
                path.end(),
                boost::next(reversePath.begin()),
                reversePath.end());
        return {bestCost, path};
    }
}

std::vector<OriginalEdgeId> Query::unpackShortcut(
        const Metric& metric,
        const Prepro& prepro,
        OverlayVertexId u,
        OverlayVertexId v,
        LevelId level) {
    // Funny story: we need to know the level to unpack the shortcut
    // but we do not need to store it in the path cache.
    if (const auto optPath = pathCache_.lookup(u, v)) {
        return *optPath;
    }

    const auto& overlay = *storage_.overlay_;
    assert(
            overlay.overlayVertexComponent(level, u) ==
            overlay.overlayVertexComponent(level, v));
    const auto componentVector = overlay.overlayVertexComponentVector(u);

    const auto component = getComponentVector(level, componentVector);

#ifndef NDEBUG
    const auto& inVertices = overlay.componentInVertices(level, component);
    assert(std::find(inVertices.begin(), inVertices.end(), u) != inVertices.end());
    const auto& outVertices = overlay.componentOutVertices(level, component);
    assert(std::find(outVertices.begin(), outVertices.end(), v) != outVertices.end());
#endif

    std::vector<std::pair<LevelId, DijkstraKey>> encodedPath;
    if (level == 0) {
        UnpackZeroLevelPolicy policy{componentVector};
        encodedPath = query(
            metric,
            prepro,
            codec_.encode(overlay.overlayVertexToOriginalEdge(u)),
            codec_.encode(overlay.overlayVertexToOriginalEdge(v)),
            policy,
            policy).second;
    } else {
        const auto& outVertices =
            overlay.componentOutVertices(level, component);
        UnpackNonzeroLevelPolicy forwardPolicy{
            level,
            *std::min_element(outVertices.begin(), outVertices.end()),
            std::max_element(outVertices.begin(), outVertices.end())->succ()};

        const auto& inVertices =
            storage_.overlay_->componentInVertices(level, component);
        UnpackNonzeroLevelPolicy reversePolicy{
            level,
            *std::min_element(inVertices.begin(), inVertices.end()),
            std::max_element(inVertices.begin(), inVertices.end())->succ()};

        encodedPath = query(
            metric,
            prepro,
            codec_.encode(u),
            codec_.encode(v),
            forwardPolicy,
            reversePolicy).second;
    }
    auto path = decodePath(metric, prepro, encodedPath);

    if (level == 0) {
        assert(path.size() >= 2);
        // Remove first and last elements.
        path.erase(path.end() - 1);
        path.erase(path.begin());
    }
    pathCache_.save(u, v, path);
    return path;
}

std::vector<OriginalEdgeId> Query::decodePath(
        const Metric& metric,
        const Prepro& prepro,
        const std::vector<std::pair<LevelId, DijkstraKey>>& encodedPath) {
    std::vector<OriginalEdgeId> path;

    for (auto i = encodedPath.begin(); i != encodedPath.end(); ++i) {
        if (storage_.codec_.isOverlayVertex(i->second)) {
            if (i == encodedPath.begin()) {
                continue;
            }
            const auto level = i->first;
            const auto vertex = storage_.codec_.overlayVertexId(i->second);
            const auto prev = boost::prior(i);
            if (codec_.isOverlayVertex(prev->second)) {
                const auto prevVertex = codec_.overlayVertexId(prev->second);
                if (!storage_.overlay_->isOutVertex(prevVertex)) {
                    assert(level == prev->first);
                    assert(level > 0);
                    const auto& chunk = unpackShortcut(
                            metric, prepro, prevVertex, vertex, level.pred());
                    path.insert(path.end(), chunk.begin(), chunk.end());
                } else {
                    const auto edgeId = storage_.overlay_->overlayOriginalEdge(
                                prevVertex).originalEdgeId;
                    if (path.empty() || path.back() != edgeId) {
                        path.push_back(edgeId);
                    }
                }
            } else {
                // Transition from original edge to overlay out vertex
                // adds nothing to the path
            }
        } else {
            const auto edgeId = codec_.originalEdgeId(i->second);
            if (path.empty() || path.back() != edgeId) {
                path.push_back(edgeId);
            }
        }
    }

    return path;
}

float Query::queryCost(
        const Metric& metric,
        const Prepro& prepro,
        OriginalEdgeId source,
        OriginalEdgeId target) {
    const auto policy = makeShortestPathPolicy(
            *storage_.graph_, *storage_.overlay_, source, target);
    return query(
            metric,
            prepro,
            codec_.encode(source),
            codec_.encode(target),
            policy,
            policy).first;
}

boost::optional<std::vector<OriginalEdgeId>> Query::queryPath(
        const Metric& metric,
        const Prepro& prepro,
        OriginalEdgeId source,
        OriginalEdgeId target) {
    const auto policy = makeShortestPathPolicy(
            *storage_.graph_, *storage_.overlay_, source, target);
    const auto& encodedPath = query(
            metric,
            prepro,
            codec_.encode(source),
            codec_.encode(target),
            policy,
            policy).second;
    if (!encodedPath.empty()) {
        return decodePath(metric, prepro, encodedPath);
    } else {
        return boost::none;
    }
}
