#include "stored_metric.h"

#include <limits>

MetricStorageBuilder::MetricStorageBuilder(const Graph* graph) :
    graph_(graph)
{
    metric_.costs_.resize(
            graph->edgesNumber(),
            std::numeric_limits<float>::infinity());
}

void MetricStorageBuilder::addCost(
        OriginalVertexId source, OriginalVertexId target, float cost) {
    if (auto edgeId = graph_->lookupEdge(source, target)) {
        metric_.costs_.at(*edgeId) = cost;

    } else {
        throw std::runtime_error(
                "MetricStorageBuilder::addCost: no such edge in the graph");
    }
}

MetricStorage<mms::Standalone> MetricStorageBuilder::build() && {
    return std::move(metric_);
}
