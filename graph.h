#pragma once

#include "tagged.h"

#include <boost/optional.hpp>

#include <cstdint>
#include <vector>

struct OriginalVertexTag;
struct OriginalEdgeTag;

using OriginalVertexId = Tagged<OriginalVertexTag, uint32_t>;
using OriginalEdgeId = Tagged<OriginalEdgeTag, uint32_t>;

struct InEdgeIndexTag;
struct OutEdgeIndexTag;

using InEdgeIndex = Tagged<InEdgeIndexTag, uint16_t>;
using OutEdgeIndex = Tagged<OutEdgeIndexTag, uint16_t>;

struct Edge {
    OriginalEdgeId id;
    OriginalVertexId source;
    OriginalVertexId target;
};

class Graph {
public:
    virtual ~Graph() {}
    virtual OriginalVertexId verticesNumber() const = 0;
    virtual OriginalEdgeId edgesNumber() const = 0;
    virtual TaggedVector<OriginalEdgeTag, Edge> edges() const = 0;
    virtual Edge edge(OriginalEdgeId edgeId) const = 0;
    virtual TaggedVector<InEdgeIndexTag, Edge> inEdges(OriginalVertexId v) const = 0;
    virtual TaggedVector<OutEdgeIndexTag, Edge> outEdges(OriginalVertexId v) const = 0;
    virtual OutEdgeIndex outEdgeIndex(OriginalEdgeId edgeId) const = 0;
    virtual InEdgeIndex inEdgeIndex(OriginalEdgeId edgeId) const = 0;

    virtual boost::optional<OriginalEdgeId> lookupEdge(
            OriginalVertexId source, OriginalVertexId target) const {
        for (const auto& e: outEdges(source)) {
            if (e.target == target) {
                return e.id;
            }
        }
        return boost::none;
    }
};
