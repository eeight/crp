#include "path_cache.h"

boost::optional<std::vector<OriginalEdgeId>> PathCache::lookup(
        OverlayVertexId u, OverlayVertexId v) const {
    auto iter = cache_.find({u, v});
    if (iter == cache_.end()) {
        return boost::none;
    } else {
        return iter->second;
    }
}

void PathCache::save(
        OverlayVertexId u, OverlayVertexId v,
        const std::vector<OriginalEdgeId>& path) {
    cache_.emplace(std::make_pair(u, v), path);
}
