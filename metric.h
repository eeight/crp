#pragma once

#include "graph.h"

#include <vector>

class Metric {
public:
    virtual float cost(OriginalEdgeId edgeId) const = 0;
    float cost(const std::vector<OriginalEdgeId>& path) const;
    virtual float turnCost(
            OriginalEdgeId edgeFromId,
            OriginalEdgeId edgeToId) const = 0;
    ~Metric() {}
};
