#include "scotch_partitioner.h"
#include "scotch.h"

#include <stdexcept>
#include <numeric>

#define VERIFY(call) do {                                   \
    if ((call) != 0) {                                      \
        throw std::runtime_error("Error calling " #call);   \
    }                                                       \
} while (false)

OneLevelPartition scotch(
        const Skeleton& skeleton, size_t partsNumber) {
    if (skeleton.verticesNumber.untag() < 1000) {
        return OneLevelPartition(skeleton.verticesNumber, 0);
    }
    // TODO Fewer memory leaks. Like seriously.
    SCOTCH_Graph graph;
    std::vector<std::pair<SCOTCH_Num, SCOTCH_Num>> edges;
    edges.reserve(edges.size()*2);
    for (const auto& edge: skeleton.edges) {
        edges.emplace_back(edge.first.untag(), edge.second.untag());
        edges.emplace_back(edge.second.untag(), edge.first.untag());
    }
    std::sort(edges.begin(), edges.end());
    edges.erase(
            std::unique(edges.begin(), edges.end()),
            edges.end());

    std::vector<SCOTCH_Num> outEdgesStart(
            skeleton.verticesNumber.untag() + 1, 0);
    std::vector<SCOTCH_Num> outVertices;
    outVertices.reserve(edges.size());
    for (const auto& edge: edges) {
        outVertices.push_back(edge.second);
        ++outEdgesStart[edge.first + 1];
    }
    std::partial_sum(
            outEdgesStart.begin(),
            outEdgesStart.end(),
            outEdgesStart.begin());

    VERIFY(SCOTCH_graphInit(&graph));
    VERIFY(SCOTCH_graphBuild(
                &graph,
                0, // baseval
                skeleton.verticesNumber.untag(),
                outEdgesStart.data(),
                outEdgesStart.data() + 1,
                nullptr, // velotab
                nullptr, // vlbltab
                edges.size(),
                outVertices.data(),
                nullptr //edlotab
                ));
    std::shared_ptr<SCOTCH_Strat> strategy(
            SCOTCH_stratAlloc(),
            &SCOTCH_memFree);
    VERIFY(SCOTCH_stratInit(strategy.get()));
    VERIFY(SCOTCH_stratGraphMapBuild(
                strategy.get(),
                SCOTCH_STRATQUALITY,
                partsNumber,
                0.01 /* imbalance */));

    std::vector<SCOTCH_Num> scotchResult(skeleton.verticesNumber.untag());
    VERIFY(SCOTCH_graphPart(
            &graph,
            partsNumber,
            strategy.get(),
            scotchResult.data()));
    OneLevelPartition result;
    result.resize(OriginalVertexId(scotchResult.size()), 0);
    size_t cross = 0;
    for (const auto& e: edges) {
        if (scotchResult[e.first] != scotchResult[e.second]) {
            ++cross;
        }
    }
    for (size_t i = 0; i != scotchResult.size(); ++i) {
        result[OriginalVertexId(i)] = GraphComponentId(scotchResult[i]);
    }

    SCOTCH_stratExit(strategy.get());
    SCOTCH_graphExit(&graph);

    return result;
}
