#pragma once

#include <algorithm>
#include <utility>
#include <vector>

template <class T>
class Heap {
public:
    using Pair = std::pair<float, T>;

    void push(T t, float w) {
        heap_.emplace_back(w, t);
        std::push_heap(
            heap_.begin(),
            heap_.end(),
            [](const Pair& lhs, const Pair& rhs) {
                return lhs.first > rhs.first;
            });
    }

    Pair pop() {
        Pair result = heap_.front();

        std::pop_heap(
            heap_.begin(),
            heap_.end(),
            [](const Pair& lhs, const Pair& rhs) {
                return lhs.first > rhs.first;
            });

        heap_.erase(heap_.end() - 1);

        return result;
    }

    bool empty() const { return heap_.empty(); }

    void clear() {
        heap_.clear();
    }

private:
    std::vector<Pair> heap_;
};
