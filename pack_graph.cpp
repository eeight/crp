#include "stored_graph.h"

#include <mms/writer.h>

#include <cstdio>
#include <fstream>

int main() {
    GraphStorageBuilder builder;
    int u, v;
    while (scanf("%d %d", &u, &v) == 2) {
        builder.addEdge(OriginalVertexId(u), OriginalVertexId(v));
    }
    std::ofstream out("genfiles/graph.mms");
    mms::safeWrite(out, builder.build());
    out.close();
    return 0;
}
