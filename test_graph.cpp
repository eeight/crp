#include "test_graph.h"

#include <random>

namespace {

const TaggedVector<OriginalEdgeTag, Edge>& allEdges() {
    static const TaggedVector<OriginalEdgeTag, Edge> edges = {
        {OriginalEdgeId(0u), OriginalVertexId(0u), OriginalVertexId(2)},
        {OriginalEdgeId(1),  OriginalVertexId(0u), OriginalVertexId(3)},
        {OriginalEdgeId(2),  OriginalVertexId(1), OriginalVertexId(2)},
        {OriginalEdgeId(3),  OriginalVertexId(1), OriginalVertexId(4)},
        {OriginalEdgeId(4),  OriginalVertexId(2), OriginalVertexId(0u)},
        {OriginalEdgeId(5),  OriginalVertexId(2), OriginalVertexId(1)},
        {OriginalEdgeId(6),  OriginalVertexId(3), OriginalVertexId(0u)},
        {OriginalEdgeId(7),  OriginalVertexId(3), OriginalVertexId(7)},
        {OriginalEdgeId(8),  OriginalVertexId(4), OriginalVertexId(1)},
        {OriginalEdgeId(9),  OriginalVertexId(4), OriginalVertexId(5)},
        {OriginalEdgeId(10), OriginalVertexId(4), OriginalVertexId(7)},
        {OriginalEdgeId(11), OriginalVertexId(5), OriginalVertexId(4)},
        {OriginalEdgeId(12), OriginalVertexId(5), OriginalVertexId(6)},
        {OriginalEdgeId(13), OriginalVertexId(6), OriginalVertexId(5)},
        {OriginalEdgeId(14), OriginalVertexId(7), OriginalVertexId(3)},
        {OriginalEdgeId(15), OriginalVertexId(7), OriginalVertexId(4)},
        {OriginalEdgeId(16), OriginalVertexId(7), OriginalVertexId(5)}};
    return edges;
}

} // namespace

OriginalVertexId TestGraph::verticesNumber() const {
    return OriginalVertexId(8);
}

OriginalEdgeId TestGraph::edgesNumber() const {
    return allEdges().size();
}

TaggedVector<OriginalEdgeTag, Edge> TestGraph::edges() const {
    return allEdges();
}

Edge TestGraph::edge(OriginalEdgeId edgeId) const {
    return allEdges()[edgeId];
}

TaggedVector<InEdgeIndexTag, Edge> TestGraph::inEdges(OriginalVertexId v) const {
    TaggedVector<InEdgeIndexTag, Edge> result;
    for (const auto& edge: allEdges()) {
        if (edge.target == v) {
            result.push_back(edge);
        }
    }

    return result;
}

TaggedVector<OutEdgeIndexTag, Edge> TestGraph::outEdges(OriginalVertexId v) const {
    TaggedVector<OutEdgeIndexTag, Edge> result;
    for (const auto& edge: allEdges()) {
        if (edge.source == v) {
            result.push_back(edge);
        }
    }

    return result;
}

OutEdgeIndex TestGraph::outEdgeIndex(OriginalEdgeId edgeId) const {
    const auto source  = allEdges()[edgeId].source;
    OutEdgeIndex index = 0;
    for (const auto& edge: allEdges()) {
        if (edge.id == edgeId) {
            return index;
        } else if (edge.source == source) {
            ++index;
        }
    }
    throw 1;
}

InEdgeIndex TestGraph::inEdgeIndex(OriginalEdgeId edgeId) const {
    const auto target  = allEdges()[edgeId].target;
    InEdgeIndex index = 0;
    for (const auto& edge: allEdges()) {
        if (edge.id == edgeId) {
            return index;
        } else if (edge.target == target) {
            ++index;
        }
    }
    throw 1;
}

TestMetric::TestMetric() {
    edgeCosts_.resize(allEdges().size(), 10);
    turnCosts_.resize(
            allEdges().size(), TaggedVector<OriginalEdgeTag, float>(allEdges().size(), 1));
}

float TestMetric::cost(OriginalEdgeId edgeId) const {
    return edgeCosts_.at(edgeId);

}

float TestMetric::turnCost(
        OriginalEdgeId edgeFromId,
        OriginalEdgeId edgeToId) const {
    return turnCosts_.at(edgeFromId).at(edgeToId);
}

void TestMetric::randomize() {
    static std::exponential_distribution<> dist(0.1);
    static std::mt19937 generator(118);

    auto r = [] {
        return dist(generator);
    };

    for (auto& w: edgeCosts_) {
        w = r();
        assert(w < std::numeric_limits<float>::infinity());
    }

    for (auto& v: turnCosts_) {
        for (auto& w: v) {
            w = r();
        }
    }
}

Partitions testGraphPartition() {
    return {
        {
            GraphComponentId(0u),
            GraphComponentId(1),
            GraphComponentId(0u),
            GraphComponentId(1),
            GraphComponentId(2),
            GraphComponentId(3),
            GraphComponentId(2),
            GraphComponentId(3)
        },
        {
            GraphComponentId(0u),
            GraphComponentId(0u),
            GraphComponentId(0u),
            GraphComponentId(0u),
            GraphComponentId(1),
            GraphComponentId(1),
            GraphComponentId(1),
            GraphComponentId(1)
        },
        {
            GraphComponentId(0u),
            GraphComponentId(0u),
            GraphComponentId(0u),
            GraphComponentId(0u),
            GraphComponentId(0u),
            GraphComponentId(0u),
            GraphComponentId(0u),
            GraphComponentId(0u)
        }};
}
