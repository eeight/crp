#pragma once

#include "overlay.h"

#include <cstdint>
#include <functional>
#include <utility>
#include <vector>

struct Skeleton {
    OriginalVertexId verticesNumber = 0;
    std::vector<std::pair<OriginalVertexId, OriginalVertexId>> edges;
};

using Partitioner = std::function<OneLevelPartition (const Skeleton&, size_t)>;

Partitions multilevelPartition(
        const Skeleton& skeleton,
        const std::vector<int>& scheme,
        const Partitioner& partitioner);
