#pragma once

#include "tagged.h"

#include <vector>
#include <tuple>

struct TinyVertexTag;

using TinyVertexId = Tagged<TinyVertexTag, uint32_t>;

class TinyGraph {
public:
    TinyGraph() {}

    // TODO reorder vertices in dfs order
    std::vector<float> bf(
            const std::vector<TinyVertexId>& inVertices,
            const std::vector<TinyVertexId>& outVertices) const;

    friend class TinyGraphBuilder;

private:
    TinyVertexId verticesNumber_ = 0;
    std::vector<size_t> outEdgesOffset_ = {0};
    std::vector<TinyVertexId> outEdges_;
    std::vector<float> outWeights_;
};

class TinyGraphBuilder {
public:
    TinyGraphBuilder() {}

    TinyVertexId addVertex();
    void addEdge(TinyVertexId source, TinyVertexId target, float weight);
    TinyGraph build();

private:
    struct Edge {
        TinyVertexId source;
        TinyVertexId target;
        float weight;

        bool operator <(const Edge& other) const {
            return std::tie(source, target) <
                std::tie(other.source, other.target);
        };
    };
    std::vector<Edge> edges_;
    TinyVertexId verticesNumber_ = 0;
};
