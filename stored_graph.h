#pragma once

#include "graph.h"
#include "mmap.h"
#include "tagged.h"

#include <mms/vector.h>

namespace detail {

struct StoredEdge {
    OriginalVertexId source;
    OriginalVertexId target;
    InEdgeIndex inIndex;

    Edge graphEdge(OriginalEdgeId id) const {
        return {id, source, target};
    }
};

} // namespace detail

namespace std {

template<>
struct is_trivial<detail::StoredEdge> : true_type {};

} // namespace std

template <class P>
class GraphStorage {
public:
    template <class A>
    void traverseFields(A&& a) const {
        a(outEdges_, outEdgesOffsets_, inEdgesIds_, inEdgesOffsets_);
    }

    OriginalVertexId verticesNumber() const {
        return outEdgesOffsets_.size().pred();
    }

    OriginalEdgeId edgesNumber() const {
        return outEdges_.size();
    }

    TaggedVector<OriginalEdgeTag, Edge> edges() const;

    Edge edge(OriginalEdgeId edgeId) const {
        return outEdges_.at(edgeId).graphEdge(edgeId);
    }

    TaggedVector<InEdgeIndexTag, Edge> inEdges(OriginalVertexId v) const;

    TaggedVector<OutEdgeIndexTag, Edge> outEdges(OriginalVertexId v) const;

    OutEdgeIndex outEdgeIndex(OriginalEdgeId edgeId) const {
        const auto source = outEdges_.at(edgeId).source;
        return retag<OutEdgeIndexTag>(
                edgeId - outEdgesOffsets_.at(source));
    }

    InEdgeIndex inEdgeIndex(OriginalEdgeId edgeId) const {
        return outEdges_.at(edgeId).inIndex;
    }

    boost::optional<OriginalEdgeId> lookupEdge(
            OriginalVertexId source, OriginalVertexId target) const;
private:
    friend class GraphStorageBuilder;

    TaggedMmsVector<P, OriginalEdgeTag, detail::StoredEdge> outEdges_;
    TaggedMmsVector<P, OriginalVertexTag, OriginalEdgeId> outEdgesOffsets_;
    TaggedMmsVector<P, OriginalEdgeTag, OriginalEdgeId> inEdgesIds_;
    TaggedMmsVector<P, OriginalVertexTag, OriginalEdgeId> inEdgesOffsets_;
};

class StoredGraph final : public Graph {
public:
    explicit StoredGraph(const std::string& filename) : mapping_(filename)
    {}

    OriginalVertexId verticesNumber() const override {
        return mapping_->verticesNumber();
    }

    OriginalEdgeId edgesNumber() const override {
        return mapping_->edgesNumber();
    }

    TaggedVector<OriginalEdgeTag, Edge> edges() const override {
        return mapping_->edges();
    }

    Edge edge(OriginalEdgeId edgeId) const override {
        return mapping_->edge(edgeId);
    }

    TaggedVector<InEdgeIndexTag, Edge> inEdges(
            OriginalVertexId v) const override {
        return mapping_->inEdges(v);
    }

    TaggedVector<OutEdgeIndexTag, Edge> outEdges(
            OriginalVertexId v) const override {
        return mapping_->outEdges(v);
    }

    OutEdgeIndex outEdgeIndex(OriginalEdgeId edgeId) const override {
        return mapping_->outEdgeIndex(edgeId);
    }

    InEdgeIndex inEdgeIndex(OriginalEdgeId edgeId) const override {
        return mapping_->inEdgeIndex(edgeId);
    }

    boost::optional<OriginalEdgeId> lookupEdge(
            OriginalVertexId source, OriginalVertexId target) const override {
        return mapping_->lookupEdge(source, target);
    }
private:
    Mapping<GraphStorage<mms::Mmapped>> mapping_;
};

class GraphStorageBuilder {
public:
    void addEdge(OriginalVertexId u, OriginalVertexId v);
    GraphStorage<mms::Standalone> build();
private:
    std::vector<std::pair<OriginalVertexId, OriginalVertexId>> edges_;
    TaggedVector<OriginalVertexTag, OutEdgeIndex> outDegrees_;
    TaggedVector<OriginalVertexTag, InEdgeIndex> inDegrees_;
};
