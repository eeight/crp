#include "stored_graph.h"
#include "range.h"

template <class P>
TaggedVector<OriginalEdgeTag, Edge> GraphStorage<P>::edges() const {
    TaggedVector<OriginalEdgeTag, Edge> result;
    result.reserve(outEdges_.size());
    for (const auto& edgeId: range(outEdges_.size())) {
        result.push_back(outEdges_.at(edgeId).graphEdge(edgeId));
    }
    return result;
}

template <class P>
TaggedVector<InEdgeIndexTag, Edge> GraphStorage<P>::inEdges(
        OriginalVertexId v) const {
    const auto begin = inEdgesOffsets_.at(v);
    const auto end = inEdgesOffsets_.at(v.succ());
    TaggedVector<InEdgeIndexTag, Edge> result;
    result.reserve(retag<InEdgeIndexTag>(end - begin));
    for (auto i = begin; i != end; ++i) {
        result.push_back(edge(inEdgesIds_.at(i)));
    }
    return result;
}

template <class P>
TaggedVector<OutEdgeIndexTag, Edge> GraphStorage<P>::outEdges(
        OriginalVertexId v) const {
    const auto begin = outEdgesOffsets_.at(v);
    const auto end = outEdgesOffsets_.at(v.succ());
    TaggedVector<OutEdgeIndexTag, Edge> result;
    result.reserve(retag<OutEdgeIndexTag>(end - begin));
    for (auto i = begin; i != end; ++i) {
        result.push_back(edge(i));
    }
    return result;
}

template <class P>
boost::optional<OriginalEdgeId> GraphStorage<P>::lookupEdge(
        OriginalVertexId source, OriginalVertexId target) const {
    const auto begin = outEdgesOffsets_.at(source);
    const auto end = outEdgesOffsets_.at(source.succ());
    for (auto i = begin; i != end; ++i) {
        if (outEdges_.at(i).target == target) {
            return i;
        }
    }
    return boost::none;
}
template class GraphStorage<mms::Mmapped>;
template class GraphStorage<mms::Standalone>;

void GraphStorageBuilder::addEdge(OriginalVertexId u, OriginalVertexId v) {
    edges_.emplace_back(u, v);
}

GraphStorage<mms::Standalone> GraphStorageBuilder::build() {
    GraphStorage<mms::Standalone> graph;
    const OriginalEdgeId edgesNumber(edges_.size());
    graph.outEdges_.reserve(edgesNumber);
    graph.inEdgesIds_.reserve(edgesNumber);

    std::sort(edges_.begin(), edges_.end());

    TaggedVector<
        OriginalVertexTag,
        TaggedVector<InEdgeIndexTag, OriginalEdgeId>> inEdges;
    graph.outEdges_.reserve(edgesNumber);
    OriginalVertexId verticesNumber = 0;
    for (const auto edgeId: range(edgesNumber)) {
        const OriginalVertexId u = edges_[edgeId.untag()].first;
        const OriginalVertexId v = edges_[edgeId.untag()].second;
        verticesNumber = std::max(verticesNumber, u.succ());
        verticesNumber = std::max(verticesNumber, v.succ());
        graph.outEdges_.push_back({u, v, 0});
        if (v >= inEdges.size()) {
            inEdges.resize(v.succ());
        }
        inEdges[v].push_back(edgeId);
    }

    graph.outEdgesOffsets_.resize(verticesNumber.succ(), 0);
    graph.inEdgesOffsets_.resize(verticesNumber.succ(), 0);

    // Fill out edges offsets
    OriginalVertexId v(0u);
    for (const auto i: range(edgesNumber)) {
        for (; v <= edges_[i.untag()].first; ++v) {
            graph.outEdgesOffsets_[v] = i;
        }
    }
    for (; v <= verticesNumber; ++v) {
        graph.outEdgesOffsets_[v] = edgesNumber;
    }

    // Fill in edges ids.
    graph.inEdgesIds_.reserve(edgesNumber);
    for (const auto& edges: inEdges) {
        for (const auto inIndex: range(edges.size())) {
            const OriginalEdgeId edgeId = edges[inIndex];
            graph.inEdgesIds_.push_back(edgeId);
            graph.outEdges_[edgeId].inIndex = inIndex;
        }
    }

    // Fill in edges offsets.
    graph.inEdgesOffsets_[OriginalVertexId(0u)] = 0;
    if (!inEdges.empty()) {
        for (const auto v: range(verticesNumber.pred())) {
            graph.inEdgesOffsets_[v.succ()] =
                graph.inEdgesOffsets_[v] +
                retag<OriginalEdgeTag>(inEdges[v].size());
        }
    }
    graph.inEdgesOffsets_[verticesNumber] = edgesNumber;

    return graph;
}

