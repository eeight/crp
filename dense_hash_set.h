#pragma once

#include "dense_hash_map.h"

// NOTE [Set on top of map]
// ~~~~~~~~~~~~~~~~~~~~~~~
// The usual way to roll both set and map with mostly shared implementation
// is to implement some kind of generic hash table and then use it in set and
// map implementations. This leaves the code littered with something like
// KeySelector, ValueSelector and so forth.
// Here I go another way and just use map with empty values as an implementation
// of set, which makes map much more readable and set just incredibly simple.
// No extra memory is consumed for empty values because internally map uses
// boost::compressed_pair for elements storage.

template <class T>
class DenseHashSet {
public:
    void insert(const T& x) {
        map_.emplace(x, {});
    }

    size_t count(const T& x) const {
        return map_.count(x);
    }

    void clear() {
        map_.clear();
    }

private:
    struct Nothing {};
    DenseHashMap<T, Nothing> map_;
};
