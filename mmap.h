#pragma once

#include <string>

#include <mms/cast.h>

class Mmap {
public:
    explicit Mmap(const std::string& filename);
    ~Mmap();

    const char* address() const { return address_; }
    size_t size() const { return size_; }

private:
    size_t size_;
    const char* address_;
};

template <class T>
class Mapping {
public:
    explicit Mapping(const std::string& filename) :
        mmap_(filename),
        value_(&mms::safeCast<T>(mmap_.address(), mmap_.size()))
    {}

    const T* get() const { return value_; }
    const T* operator ->() const { return value_; }

private:
    Mmap mmap_;
    const T* value_;
};
