#pragma once

#include "tagged.h"

#include <boost/compressed_pair.hpp>

#include <cassert>
#include <cstdint>
#include <limits>
#include <vector>

template <class T>
struct Hash;

template <>
struct Hash<uint32_t> {
    size_t operator()(uint32_t x) const {
        x ^= x >> 16;
        x *= 0x85ebca6b;
        x ^= x >> 13;
        x *= 0xc2b2ae35;
        x ^= x >> 16;
        return x;
    }
};

template <class Tag, class T>
struct Hash<Tagged<Tag, T>> {
    size_t operator()(Tagged<Tag, T> x) const {
        return Hash<T>()(x.untag());
    }
};

template <class T>
struct Empty {
    T operator()() const { return {}; }
};

template <class Tag, class T>
struct Empty<Tagged<Tag, T>> {
    Tagged<Tag, T> operator()() const {
        // A bit presumptious, but will do for our applications.
        return std::numeric_limits<Tagged<Tag, T>>::max();
    }
};

template <class Key, class Value>
class DenseHashMap {
public:
    explicit DenseHashMap(size_t initialBuckets = 32) :
        buckets_(initialBuckets, {Empty<Key>()(), Empty<Value>()()})
    {
        assert(!buckets_.empty());
        assert((buckets_.size() & (buckets_.size() - 1)) == 0);
    }

    size_t count(const Key& key) const {
        return find(key).first;
    }

    const Value& at(const Key& key) const {
        const auto info = find(key);
        if (!info.first) {
            throw std::logic_error("DenseHashMap::at: key not found");
        }
        return buckets_[info.second].second();
    }

    Value& at(const Key& key) {
        // This is safe. Casting away const on a value that is guaranteed to
        // be non-const.
        return const_cast<Value&>(
                const_cast<const DenseHashMap *>(this)->at(key));
    }

    Value& operator[](const Key& key) {
        auto info = find(key);
        if (!info.first) {
            buckets_[info.second] = {key, Empty<Value>()()};
            if (increaseSize()) {
                // Need to find new index after growing the table.
                info = find(key);
            }
        }
        return buckets_[info.second].second();
    };

    void emplace(const Key& key, const Value& value) {
        const auto info = find(key);
        if (!info.first) {
            buckets_[info.second] = {key, value};
            increaseSize();
        }
    }

    size_t size() const { return size_; }

    void clear() {
        size_ = 0;
        buckets_.assign(buckets_.size(), {Empty<Key>()(), Empty<Value>()()});
    }

    void shrink() {
        clear();
        buckets_.shrink_to_fit();
    }

private:
    // When the key is found, returns {true, bucket number where the key is}
    // Otherwise returns {false, bucket number where the key might be inserted}
    std::pair<bool, size_t> find(const Key& key) const {
        const size_t mask = (buckets_.size() - 1);
        for (size_t i = Hash<Key>()(key) & mask; true; i = (i + 1) & mask) {
            const auto bucketKey = buckets_[i].first();
            if (bucketKey == Empty<Key>()()) {
                return {false, i};
            } else if (bucketKey == key) {
                return {true, i};
            }
        }
    }

    bool increaseSize() {
        ++size_;

        // Hardcoded max fill factor of .75.
        if (size_*4 > buckets_.size()*3) {
            grow();
            return true;
        } else {
            return false;
        }
    }

    void grow() {
        DenseHashMap grown(buckets_.size()*2);
        for (const auto& bucket: buckets_) {
            if (bucket.first() != Empty<Key>()()) {
                grown.emplace(bucket.first(), bucket.second());
            }
        }
        std::swap(buckets_, grown.buckets_);
    }

    size_t size_ = 0;
    // Compressed pair here is for DenseHashSet implementation.
    // See [Set on top of map] in dense_hash_set.h
    std::vector<boost::compressed_pair<Key, Value>> buckets_;
};
