#pragma once

#include "graph.h"
#include "metric.h"
#include "mmap.h"
#include "overlay.h"
#include "printer.h"
#include "tagged.h"

#include <cstdint>
#include <vector>

class Prepro {
public:
    virtual const float* overlayCosts(uint32_t offset) const = 0;
    virtual const float* turnTable(OriginalVertexId vertex) const = 0;
    virtual ~Prepro() {}
};

template <class P>
class PreproStorage {
public:
    template <class A>
    void traverseFields(A&& a) const {
        a(overlayCosts_, turnTableOffsets_, turnTablesCosts_);
    }

    friend class PreproBuilder;

    const float* overlayCosts(uint32_t offset) const {
        return &overlayCosts_[0] + offset;
    }

    const float* turnTable(OriginalVertexId vertex) const {
        return {&turnTablesCosts_[turnTableOffsets_[vertex]]};
    }

    void print(Printer* printer) const {
        printer->begin("prepro");
        printer->key("overlayCosts_");
        printer->value(overlayCosts_);
        printer->key("turnTableOffsets_");
        printer->value(turnTableOffsets_);
        printer->key("turnTablesCosts_");
        printer->value(turnTablesCosts_);
        printer->end();
    }
private:
    // Contiguous array of all overlay edges costs.
    mms::vector<P, float> overlayCosts_;
    TaggedMmsVector<P, OriginalVertexTag, uint32_t> turnTableOffsets_;
    mms::vector<P, float> turnTablesCosts_;
};

class StoredPrepro final : public Prepro {
public:
    explicit StoredPrepro(const std::string& filename) :
        mapping_(filename)
    {}

    const float* overlayCosts(uint32_t offset) const override {
        return mapping_->overlayCosts(offset);
    }

    const float* turnTable(OriginalVertexId vertex) const override {
        return mapping_->turnTable(vertex);
    }

private:
    Mapping<PreproStorage<mms::Mmapped>> mapping_;
};

PreproStorage<mms::Standalone> buildPrepro(
        const Graph& graph,
        const Overlay& overlay,
        const Metric& metric);

