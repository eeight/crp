#pragma once

#include "metric.h"
#include "mmap.h"

template <class P>
class MetricStorage {
public:
    template <class A>
    void traverseFields(A&& a) const {
        a(costs_);
    }

    float cost(OriginalEdgeId edgeId) const {
        return costs_.at(edgeId);
    }

private:
    friend class MetricStorageBuilder;

    TaggedMmsVector<P, OriginalEdgeTag, float> costs_;
};

class StoredMetric final : public Metric {
public:
    explicit StoredMetric(const std::string& filename) :
        mapping_(filename)
    {}

    float cost(OriginalEdgeId edgeId) const override {
        return mapping_->cost(edgeId);
    }

    float turnCost(OriginalEdgeId, OriginalEdgeId) const override {
        return 1.0;
    }
private:
    Mapping<MetricStorage<mms::Mmapped>> mapping_;
};

class MetricStorageBuilder {
public:
    explicit MetricStorageBuilder(const Graph* graph);
    void addCost(OriginalVertexId source, OriginalVertexId target, float cost);
    MetricStorage<mms::Standalone> build() &&;

private:
    const Graph* graph_;
    MetricStorage<mms::Standalone> metric_;
};
