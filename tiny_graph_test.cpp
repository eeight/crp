#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include "debug_io.h"
#include "floyd_warshall.h"
#include "graph.h"
#include "range.h"
#include "test_graph.h"
#include "tiny_graph.h"

#include <boost/test/unit_test.hpp>

std::pair<TinyGraph, TaggedVector<OriginalEdgeTag, TinyVertexId>>
toTiny(const Graph& graph, const Metric& metric) {
    TaggedVector<OriginalEdgeTag, TinyVertexId> tinyVertices;
    TinyGraphBuilder builder;
    for (OriginalEdgeId i = 0; i != graph.edgesNumber(); ++i) {
        tinyVertices.push_back(builder.addVertex());
    }

    for (const auto& e: graph.edges()) {
        for (const auto& outE: graph.outEdges(e.target)) {
            builder.addEdge(
                    tinyVertices.at(e.id),
                    tinyVertices.at(outE.id),
                    metric.cost({e.id, outE.id}));
        }
    }

    return {builder.build(), tinyVertices};
}

BOOST_AUTO_TEST_CASE(bf_test) {
    TestGraph graph;
    TestMetric metric;

    TinyGraph tinyGraph;
    TaggedVector<OriginalEdgeTag, TinyVertexId> vertices;
    std::tie(tinyGraph, vertices) = toTiny(graph, metric);
    const auto answer = fw(graph, metric);

    // 1. one-one
    for (const auto source: graph.edges()) {
        for (const auto target: graph.edges()) {
            const float bfResult = tinyGraph.bf(
                    {vertices.at(source.id)},
                    {vertices.at(target.id)}).front();
            BOOST_TEST_REQUIRE(answer[source.id][target.id] == bfResult);
        }
    }

    // 2. one-all
    for (const auto source: graph.edges()) {
        BOOST_TEST(
                static_cast<const std::vector<float>&>(answer[source.id]) ==
                tinyGraph.bf(
                    {vertices.at(source.id)},
                    vertices),
                boost::test_tools::per_element());
    }

    // 3. all-one
    for (const auto target: graph.edges()) {
        std::vector<float> expected;
        for (const auto source: graph.edges()) {
            expected.push_back(answer[source.id][target.id]);
        }
        BOOST_TEST(
                expected ==
                tinyGraph.bf(
                    vertices,
                    {vertices.at(target.id)}),
                boost::test_tools::per_element());
    }

    // 2. all-all
    std::vector<float> expected;
    for (const auto source: graph.edges()) {
        for (const auto target: graph.edges()) {
            expected.push_back(answer[source.id][target.id]);
        }
    }
    BOOST_TEST(
            expected == tinyGraph.bf(vertices, vertices),
            boost::test_tools::per_element());
}
