#include "mmap.h"
#include "overlay.h"
#include "prepro.h"
#include "stored_graph.h"
#include "stored_metric.h"

#include <mms/writer.h>
#include <mms/cast.h>

#include <fstream>

int main() {
    Mmap overlayMapping("genfiles/graph_overlay.mms");
    StoredMetric metric("genfiles/graph_metric.mms");
    auto overlay = mms::safeCast<Overlay>(
            overlayMapping.address(), overlayMapping.size());
    StoredGraph graph("genfiles/graph.mms");

    const auto prepro = buildPrepro(graph, overlay, metric);
    std::ofstream out("genfiles/graph_prepro.mms");
    mms::safeWrite(out, prepro);
    out.close();
    return 0;
}
