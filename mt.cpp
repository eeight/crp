#include "mt.h"

#include <cassert>

ThreadPool::ThreadPool(size_t threads) {
    for (size_t i = 0; i != threads; ++i) {
        threads_.emplace_back([this] { worker(); });
    }
}

void ThreadPool::push(std::function<void ()> task) {
    {
        std::unique_lock<std::mutex> lock(mutex_);
        stack_.push(task);
    }
    haveWork_.notify_one();
}

void ThreadPool::finalize() {
    {
        std::unique_lock<std::mutex> lock(mutex_);
        stopping_ = true;
    }
    haveWork_.notify_all();
    for (auto& thread: threads_) {
        thread.join();
    }
}

void ThreadPool::worker() {
    for (;;) {
        std::unique_lock<std::mutex> lock(mutex_);
        haveWork_.wait(
                lock,
                [this] { return !stack_.empty() || stopping_; });
        if (stack_.empty() && stopping_) {
            break;
        }
        assert(!stack_.empty());
        auto next = std::move(stack_.top());
        stack_.pop();
        lock.unlock();
        next();
    }
}

Dispatcher::Dispatcher() :
    pool_(std::thread::hardware_concurrency())
{}

void Dispatcher::schedule(Task* task) {
    if (task->runnable()) {
        doSchedule(task);
    }
}

void Dispatcher::run() {
    pool_.finalize();
}

void Dispatcher::doSchedule(Task* task) {
    pool_.push([this, task] {
        task->run();
        if (Task* dep = task->dependent()) {
            if (dep->prereqDone()) {
                doSchedule(dep);
            }
        }
    });
}
