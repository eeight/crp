#include "mmap.h"
#include "overlay.h"
#include "partition.h"
#include "scotch_partitioner.h"
#include "stored_graph.h"

#include <mms/cast.h>
#include <mms/writer.h>

#include <fstream>

Partitions partition(const Graph& graph) {
    Skeleton skeleton;
    skeleton.verticesNumber = graph.verticesNumber();
    const auto& edges = graph.edges();
    std::cout << "Creating skeleton...\n";
    skeleton.edges.reserve(edges.size().untag());
    for (const auto& edge: edges) {
        skeleton.edges.emplace_back(edge.source, edge.target);
    }
    return multilevelPartition(skeleton, {16, 8, 8, 4, 4}, &scotch);
}

template <class P>
using MmsType = mms::vector<P, mms::vector<P, uint32_t>>;

void savePartition(const Partitions& partitions) {
    auto data = reinterpret_cast<const MmsType<mms::Standalone>*>(&partitions);
    std::ofstream out("genfiles/partition.mms");
    mms::safeWrite(out, *data);
    out.close();
}

Partitions loadPartition() {
    Mmap mapping("genfiles/partition.mms");
    auto data = mms::safeCast<const MmsType<mms::Mmapped>>(
            mapping.address(), mapping.size());
    Partitions partitions;
    partitions.resize(LevelId(data.size()), OneLevelPartition(
                    OriginalVertexId(data[0].size()), 0));
    for (size_t i = 0; i != data.size(); ++i) {
        for (size_t j = 0; j != data[i].size(); ++j) {
            partitions[LevelId(i)][OriginalVertexId(j)] =
                GraphComponentId(data[i][j]);
        }
    }
    return partitions;
}

int main() {
    StoredGraph graph("genfiles/graph.mms");
    Partitions partitions;
    try {
        partitions = loadPartition();
    } catch (...) {
        partitions = partition(graph);
        savePartition(partitions);
    }
    const auto overlay = buildOverlay(graph, partitions);
    std::ofstream out("genfiles/graph_overlay.mms");
    mms::safeWrite(out, overlay);
    out.close();
    return 0;
}
