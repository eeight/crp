#include "test_graph.h"
#include "prepro.h"
#include "overlay.h"
#include "mmap.h"

#include <mms/writer.h>
#include <mms/cast.h>

#include <fstream>

int main() {
    TestGraph graph;
    TestMetric metric;
    Mmap overlayMapping("genfiles/overlay.mms");
    auto overlay = mms::safeCast<Overlay>(
            overlayMapping.address(), overlayMapping.size());
    const auto prepro = buildPrepro(graph, overlay, metric);
    Printer printer;
    prepro.print(&printer);
    std::ofstream out("genfiles/prepro.mms");
    mms::safeWrite(out, prepro);
    out.close();
    return 0;
}
