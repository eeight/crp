#pragma once

#include "graph.h"
#include "metric.h"
#include "overlay.h"

class TestGraph : public Graph {
public:
    OriginalVertexId verticesNumber() const override;
    OriginalEdgeId edgesNumber() const override;
    TaggedVector<OriginalEdgeTag, Edge> edges() const override;
    Edge edge(OriginalEdgeId edgeId) const override;
    TaggedVector<InEdgeIndexTag, Edge> inEdges(OriginalVertexId v) const override;
    TaggedVector<OutEdgeIndexTag, Edge> outEdges(OriginalVertexId v) const override;
    OutEdgeIndex outEdgeIndex(OriginalEdgeId edgeId) const override;
    InEdgeIndex inEdgeIndex(OriginalEdgeId edgeId) const override;
};

class TestMetric : public Metric {
public:
    TestMetric();
    float cost(OriginalEdgeId edgeId) const override;
    float turnCost(
            OriginalEdgeId edgeFromId,
            OriginalEdgeId edgeToId) const override;
    void randomize();

private:
    TaggedVector<OriginalEdgeTag, float> edgeCosts_;
    TaggedVector<OriginalEdgeTag, TaggedVector<OriginalEdgeTag, float>> turnCosts_;
};

Partitions testGraphPartition();
