#pragma once

#include "graph.h"
#include "overlay.h"

#include <boost/optional.hpp>

#include <map>
#include <utility>
#include <vector>

class PathCache {
public:
    boost::optional<std::vector<OriginalEdgeId>> lookup(
            OverlayVertexId u, OverlayVertexId v) const;
    void save(
            OverlayVertexId u, OverlayVertexId v,
            const std::vector<OriginalEdgeId>& path);
private:
    std::map<
        std::pair<OverlayVertexId, OverlayVertexId>,
        // TODO better path encoding.
        std::vector<OriginalEdgeId>> cache_;
};
