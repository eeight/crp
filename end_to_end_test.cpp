#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK

#include "test_graph.h"
#include "graph.h"
#include "overlay.h"
#include "prepro.h"
#include "range.h"
#include "query.h"

#include <mms/cast.h>
#include <mms/writer.h>

#include <boost/test/unit_test.hpp>

#include <limits>
#include <sstream>

template <class Tag, class T>
std::ostream& operator <<(std::ostream& out, Tagged<Tag, T> t) {
    return out << t.untag();
}

// Floyd-Warshall algorithm with turn costs.
TaggedVector<OriginalEdgeTag, TaggedVector<OriginalEdgeTag, float>>
fw(const Graph& graph, const Metric& metric) {
    TaggedVector<OriginalEdgeTag, TaggedVector<OriginalEdgeTag, float>> costs(
            graph.edgesNumber(),
            TaggedVector<OriginalEdgeTag, float>(
                graph.edgesNumber(), std::numeric_limits<float>::infinity()));

    for (const auto& e: graph.edges()) {
        costs[e.id][e.id] = 0;
        for (const auto& outE: graph.outEdges(e.target)) {
            costs[e.id][outE.id] = metric.turnCost(e.id, outE.id) +
                metric.cost(outE.id);
        }
    }

    for (const auto k: range(graph.edgesNumber())) {
        for (const auto i: range(graph.edgesNumber())) {
            for (const auto j: range(graph.edgesNumber())) {
                costs[i][j] = std::min(
                    costs[i][j], costs[i][k] + costs[k][j]);
            }
        }
    }

    return costs;
}

template <template <class> class T>
const T<mms::Mmapped>* freeze(const T<mms::Standalone>& t) {
    std::stringstream out;
    mms::safeWrite(out, t);
    // The memory leak is deliberate. Here it helps to make
    // the interface simpler.
    std::string* storage = new std::string(out.str());
    return &mms::safeCast<T<mms::Mmapped>>(storage->c_str(), storage->size());
}

class TestPrepro final : public Prepro {
public:
    explicit TestPrepro(const PreproStorage<mms::Mmapped>* storage) :
        storage_(storage)
    {}

    const float* overlayCosts(uint32_t offset) const override {
        return storage_->overlayCosts(offset);
    }

    const float* turnTable(OriginalVertexId vertex) const override {
        return storage_->turnTable(vertex);
    }

private:
    const PreproStorage<mms::Mmapped>* storage_;
};

void verify(
        const Graph& graph,
        const Partitions& partitions,
        const Metric& metric) {
    auto overlay = freeze(buildOverlay(graph, partitions));
    auto preproStorage = freeze(buildPrepro(graph, *overlay, metric));
    TestPrepro prepro(preproStorage);

    Query q(&graph, overlay);
    const auto correctCosts = fw(graph, metric);
    for (const auto& source: graph.edges()) {
        for (const auto& target: graph.edges()) {
            BOOST_TEST_REQUIRE(correctCosts[source.id][target.id] ==
                    q.queryCost(metric, prepro, source.id, target.id));
            const auto path =
                    q.queryPath(metric, prepro, source.id, target.id);
            BOOST_TEST_REQUIRE(static_cast<bool>(path));
            BOOST_TEST(correctCosts[source.id][target.id] ==
                    metric.cost(*path));
            BOOST_TEST(path->front() == source.id);
            BOOST_TEST(path->back() == target.id);
            // Check that transitions between path edges are possible.
            for (size_t i = 1; i != path->size(); ++i) {
                BOOST_TEST_REQUIRE(graph.edge(path->at(i - 1)).target ==
                        graph.edge(path->at(i)).source);
            }
        }
    }
}

BOOST_AUTO_TEST_CASE(test_trivial_metric, *boost::unit_test::tolerance(1e-5f)) {
    verify(TestGraph{}, testGraphPartition(), TestMetric{});
}

BOOST_AUTO_TEST_CASE(test_random_metrics, *boost::unit_test::tolerance(1e-5f)) {
    TestMetric metric;
    for (size_t i = 0; i != 100; ++i) {
        metric.randomize();
        verify(TestGraph{}, testGraphPartition(), metric);
    }
}
