#include "overlay.h"
#include "test_graph.h"

#include <mms/writer.h>

#include <fstream>

int main() {
    TestGraph graph;
    const auto overlay = buildOverlay(graph, testGraphPartition());
    Printer printer;
    overlay.print(&printer);
    std::ofstream out("genfiles/overlay.mms");
    mms::safeWrite(out, overlay);
    out.close();
    return 0;
}
